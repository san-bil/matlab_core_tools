function [ label_smoothed_output,medianed_smoothed_act_upsampled,output ] = my_vad( input,fs,downsampler )

proxy_signal = abs(input(1:downsampler:end));
proxy_fs = fs/downsampler;

window_size=(proxy_fs)/10;
assignment_grp_size=(proxy_fs)/20;

act=(quantfilt1(proxy_signal,round(window_size),round(assignment_grp_size),1,0.9));

if(mod(window_size,2)==0)
    window_size=window_size+1;
end

smoothed_act = sgolayfilt(act,3,window_size);

medianed_smoothed_act=(quantfilt1(smoothed_act,proxy_fs/2,proxy_fs/2,1,0.9));
medianed_smoothed_act_upsampled = resample(medianed_smoothed_act,fs,proxy_fs);

global_output_median = quantile(medianed_smoothed_act,0.4);
output = medianed_smoothed_act_upsampled>global_output_median;

label_smoothed_output = agglomerate_sequence_labels(output,fs*0.5);