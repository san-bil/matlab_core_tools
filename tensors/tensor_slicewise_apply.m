function [ out_tensor ] = tensor_slicewise_apply( tensor,func_handle )

output_size = size(func_handle(tensor(:,:,1)));

out_tensor = zeros(output_size(1),output_size(2),size(tensor,3));

for i=1:size(tensor,3)
    
        slice=tensor(:,:,i);
        processed_slice = func_handle(slice);
        out_tensor(:,:,i)=processed_slice;

end

