function out = flatten_points(in)

out = reshape(permute(in,[2 1 3]), [size(in,1)*size(in,2) size(in,3) ])';
