function [ applied_tensor ] = time_tensor_applyfun( tensor, function_handle )

% in: a spatiotemporal volume (a 3 tensor)
%     a function handle
%
% out: the spatiotemporal volume/tensor, with the function handle applied to each vector looking in the 3-indexing-direction i.e. FOR_ALL x,y tensor(x,y,:)
%
% desc: as above
%
% tags: #tensor #apply #smoothing #cleaning

applied_tensor = zeros(size(tensor));
for i=1:size(tensor,1)
    for j=1:size(tensor,2)
        vec=tensor(i,j,:);
        rotated_vec = permute(vec,[3,1,2]);
        applied_vec = function_handle(rotated_vec);
        applied_tensor(i,j,:)=applied_vec;
    end
end



end

