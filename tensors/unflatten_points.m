function out = unflatten_points(in,num_points_per_image, point_dim)


if(~exist('point_dim','var'))
    point_dim=2;
end

out = reshape(in',point_dim, num_points_per_image, []);
out = permute(out,[2 1 3]);
