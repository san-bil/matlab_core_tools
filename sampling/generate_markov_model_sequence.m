function [ state_sequence] = generate_markov_model_sequence( state_set, initial_state, transition_matrix,sample_length )

% in: a list representing the (discrete) state space of the system
%     the initial state 
%     the transition matrix of the system
%     the desired sequence length
% out: a state sequence for the given system 
%
% desc: simply simulates a markov model in a discrete state space
%
% tags: #simulation #markovmodel #generation

curr_state_idx=find(state_set==initial_state);

state_sequence = zeros(sample_length,1);

for i = 1:sample_length

    state_sequence(i) = state_set(curr_state_idx);
    
    curr_state_transition_pdf = transition_matrix(curr_state_idx,:);
    
    curr_state_transition_cdf = cumsum(curr_state_transition_pdf);
    
    curr_state_idx = head(find(rand(1,1)<curr_state_transition_cdf));
    
end


end

