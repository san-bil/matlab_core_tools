function [dataframe,other_outputs_acc] = ds_apply_to_columns_blockwise(chunking_column_name, columns_name, func_handle, datastack)


[dataframe,other_outputs_acc] = apply_to_columns_blockwise(chunking_column_name, columns_name,datastack.dictionary,func_handle, datastack.dataframe);