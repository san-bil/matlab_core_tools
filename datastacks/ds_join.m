function [ datastack_out ] = ds_join( datastack_1, datastack_2 )

datastack_out = ds_init();

[new_dataframe,added_features_index] = add_to_dataframe(datastack_1.dataframe, datastack_2.dataframe);
datastack_2_dict_offset = added_features_index(1);
datastack_2_dict_offsetted = offset_data_dictionary(datastack_2_dict_offset,datastack_2.dictionary);
new_data_dictionary = kv_join(datastack_1.dictionary,datastack_2_dict_offsetted);

datastack_out.dataframe = new_dataframe;
datastack_out.dictionary= new_data_dictionary;


end

