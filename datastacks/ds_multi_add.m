function [datastack] = ds_multi_add( datastack, varargin )

% in: dataset (rows=samples, columns=variables);
%     new columns to add to dataset
%
% out: new dataset, with columns added;
%      indices in new dataset of the added columns, for book-keeping (i.e. constructing a data dictionary)
%
% desc: adds the columns to the dataset by horizontal concatenation, 
% but also tracks the column numbers in the new dataset of the added columns, should you
% want to construct a data dictionary 
%
% tags: #dataset #horzcat #bookkeeping #metadata

[dataframe,new_data_dictionary] = add_to_dataframe_multi( datastack.dataframe, varargin );
 
datastack.frame = dataframe;
datastack.dictionary = new_data_dictionary;