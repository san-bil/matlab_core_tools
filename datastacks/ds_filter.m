function [out,matching_rows_idxs]=ds_filter(col_key,filter_fhandle,datastack)

[out,matching_rows_idxs]=filter_dataframe(col_key,datastack.data_dictionary,filter_fhandle,datastack.dataframe);

end
