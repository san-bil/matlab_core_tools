function [ out ] = ds_sanity( datastack )
%DS_SANITY Summary of this function goes here
%   Detailed explanation goes here

idxs = kv_get_vals(datastack.dictionary);
stacked_idxs=cell2mat(cellfun(@transpose,idxs,'UniformOutput',0));
unique_idxs = unique(stacked_idxs);
out = (unique_idxs == size(datastack.dataframe,2));




end

