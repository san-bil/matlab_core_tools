function out=ds_get_column_uniques(key,datastack)

out=get_dataframe_column_uniques(key,datastack.dictionary,datastack.dataframe);

end
