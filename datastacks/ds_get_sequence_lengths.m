function out = ds_get_sequence_lengths(datastack)

out = get_dataframe_sequence_lengths(datastack.dictionary, datastack.dataframe);