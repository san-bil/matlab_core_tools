function updated_datastack = ds_add_columns( datastack, columns_name, columns_to_add )


[updated_dataframe,column_numbers] = add_to_dataframe( datastack.dataframe, columns_to_add );

updated_datastack=ds_init();
updated_datastack.dictionary = kv_set(columns_name, column_numbers, datastack.dictionary);
updated_datastack.dataframe = updated_dataframe;
