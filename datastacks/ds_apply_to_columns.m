function [dataframe] = ds_apply_to_columns(columns_name,func_handle,datastack)

[dataframe] = apply_to_columns(columns_name,datastack.dictionary,func_handle,datastack.dataframe);