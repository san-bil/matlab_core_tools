function [actualSequenceLabels, sequenceIDs, processedSubjectColumn]  = collapseFramesToSequences(ground_truth, sequence_column, subject_column)

sequenceIDs = unique(sequence_column);
actualSequenceLabels = zeros(size(sequenceIDs,1),1);
processedSubjectColumn = zeros(size(sequenceIDs,1),1);
           
for i = 1:length(sequenceIDs)
    [blockStart, blockEnd] = findValueBlock(sequenceIDs(i), sequence_column);
    sequenceActuals = ground_truth(blockStart:blockEnd);
    
%     sequencePredictedLabel = mode(sequencePredictions);
%     sequenceActualLabel = mode(sequenceActuals);
    
	sequenceActualLabel = round(sum(sequenceActuals)/length(sequencePredictions));
    
    assert(sum(sequenceActuals==sequenceActualLabel)==length(sequenceActuals));
    
    subj = subject_column(blockStart);
    
    assert(length(unique(subject_column(blockStart:blockEnd)))==1 && unique(subject_column(blockStart:blockEnd))==subject_column(blockStart));
    
    processedSubjectColumn(i)=subj;
    actualSequenceLabels(i)=sequenceActualLabel; 
end

end
