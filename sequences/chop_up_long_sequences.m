function sequence_column = chop_up_long_sequences(ground_truth_column,orig_sequence_column,dist_object,sequence_length_lower_bound)
sequence_column = orig_sequence_column;
class_label_set = unique(ground_truth_column);
assert(length(class_label_set)==2) %assume this is for binary classification, as this function is to be used to mitigate class imbalance and sequence length imbalance

smaller_class_label = class_label_set((sum(ground_truth_column==class_label_set(1))>sum(ground_truth_column==class_label_set(2)))+1);%label of the class with fewer samples
% find mean and variance of mimicry-sequences, assume gamma distribution
larger_class_label = setdiff(class_label_set,smaller_class_label);

if(~exist('dist_object','var'))
    dist_object = fitdist(get_sequence_lengths(sequence_column(ground_truth_column==smaller_class_label)),'Gamma');
end
    
if(~exist('sequence_length_lower_bound','var'))
    sequence_length_lower_bound = 20;  
end

smaller_class_sequence_length_mean = dist_object.mean();

majority_class_sequences = sequence_column(ground_truth_column==larger_class_label);

long_sequences = find_long_sequences(majority_class_sequences,round(3*smaller_class_sequence_length_mean));

debug_acc = [];

for i=1:length(long_sequences)
    long_sequence = long_sequences(i);
    sequence_idxs = find(orig_sequence_column==long_sequence);
    sequence_start = sequence_idxs(1);
    subsequence_lengths = [];
    
    while(true)
        new_seq_length = round(dist_object.random());
        if(new_seq_length>sequence_length_lower_bound)
            if(sum(subsequence_lengths)+new_seq_length>length(sequence_idxs))
                remainder_seq_length = length(sequence_idxs) - sum(subsequence_lengths);
                if(remainder_seq_length>20)
                    subsequence_lengths = [subsequence_lengths; remainder_seq_length];
                else
                    subsequence_lengths(end) = subsequence_lengths(end)+remainder_seq_length;
                end
                break;
            else
                subsequence_lengths = [subsequence_lengths; new_seq_length];
            end
        end
    end
    subsequence_lengths(subsequence_lengths==0) = [];
    debug_acc = [debug_acc; subsequence_lengths];
    subsequence_sub_idxs = cumsum(subsequence_lengths);
    subsequence_sub_idxs = subsequence_sub_idxs(1:end-1);
    
    for j = 1:length(subsequence_sub_idxs)
        session_suffix_idx = sequence_start+subsequence_sub_idxs(j);
        sequence_column(session_suffix_idx:end) = sequence_column(session_suffix_idx:end)+1;
        test=1;
    end
        
end

