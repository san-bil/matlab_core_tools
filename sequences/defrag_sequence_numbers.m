function defragged_dataframe = defrag_sequence_numbers(dataframe, data_dictionary)

sequence_idx = kv_get('sequence_idx', data_dictionary);


if(~isempty(dataframe))

    sequence_column = dataframe(:,sequence_idx);
    defragged_sequence_column = defrag_sequence_column(sequence_column);
   
	defragged_dataframe = dataframe;
    defragged_dataframe(:,sequence_idx) = defragged_sequence_column;
    
else
    defragged_dataframe = dataframe;
end