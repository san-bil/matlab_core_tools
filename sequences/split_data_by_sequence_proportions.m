function [training_set, validation_set, test_set, data_dictionary] = split_data_by_sequence_proportions(split_ratios,dataframe,data_dictionary)

if(~exist('split_ratios','var'))
    split_ratios=[2 1 1];
end

ground_truth_column=get_dataframe_column('ground_truth_idx',data_dictionary,dataframe);
dummy_sequence_numbers = generate_dummy_sequence_numbers(ground_truth_column);
chopped_dummy_sequence_col = chop_up_long_sequences(ground_truth_column,dummy_sequence_numbers);

[dataframe,sequence_idx] = add_to_dataframe(dataframe,chopped_dummy_sequence_col);
data_dictionary = kv_set('sequence_idx', sequence_idx, data_dictionary);

sequence_numbers = get_dataframe_column_uniques('sequence_idx',data_dictionary,dataframe);
sequence_numbers_splits = split_data_by_proportion(sequence_numbers, split_ratios, {'training','validation','testing'});

training_set = filter_dataframe('sequence_idx',data_dictionary,@(x)ismember(x,kv_get('training',sequence_numbers_splits)),dataframe);
validation_set = filter_dataframe('sequence_idx',data_dictionary,@(x)ismember(x,kv_get('validation',sequence_numbers_splits)),dataframe);
test_set = filter_dataframe('sequence_idx',data_dictionary,@(x)ismember(x,kv_get('testing',sequence_numbers_splits)),dataframe);

[training_set,set_idx] = add_constant_column(training_set,1);
[validation_set,set_idx] = add_constant_column(validation_set,2);
[test_set,set_idx] = add_constant_column(test_set,3);

data_dictionary = kv_set('set_idx', set_idx, data_dictionary);

end