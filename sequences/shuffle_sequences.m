function [ compacted_shuffled_dataset ] = shuffle_sequences( dataset, data_dictionary )

%TODO: verify this!

dataset(:,kv_get('sequence_idx',data_dictionary)) = safe_reset_sequence_numbers(dataset(:,kv_get('sequence_idx',data_dictionary)));
sequence_column = dataset(:,kv_get('sequence_idx',data_dictionary));
assert(isempty(find(diff(sequence_column)<0, 1)));

sequence_numbers = my_unique(sequence_column);
sequence_lengths = get_sequence_lengths(sequence_column);

new_order = randperm(length(sequence_numbers));
new_order_sequence_lengths = sequence_lengths(new_order);

compacted_shuffled_dataset=zeros(size(dataset));

marker = 1;

for i = 1:length(new_order)
    
    sequence_length = new_order_sequence_lengths(i);
    compacted_shuffled_dataset(marker:marker+sequence_length-1,:) = dataset(sequence_column==new_order(i),:);
    marker = marker+sequence_length;
end

compacted_shuffled_dataset(:,kv_get('sequence_idx',data_dictionary)) = safe_reset_sequence_numbers(compacted_shuffled_dataset(:,kv_get('sequence_idx',data_dictionary)));


end

