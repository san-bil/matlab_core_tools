function stacked_sequences = bag_to_stacked_sequences(sequences_bag)

sequences_lengths = force_row_vec(get_sequences_bag_lengths(sequences_bag));
stacked_sequences = zeros(sum(sequences_lengths),size(sequences_bag{1},2));

split_points = [1 cumsum(sequences_lengths)+1];

for i = 1:length(sequences_bag)
    stacked_sequences(split_points(i):(split_points(i+1)-1),:) = sequences_bag{i};
end
