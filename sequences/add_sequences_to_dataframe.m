function updated_dataframe = add_sequences_to_dataframe(dataframe, new_samples, data_dictionary)

if(~isempty(dataframe))
sequence_idx = kv_get('sequence_idx', data_dictionary);
new_samples(:,sequence_idx) = new_samples(:,sequence_idx)+max(dataframe(:,sequence_idx));
updated_dataframe = [dataframe;new_samples];
else
    updated_dataframe = new_samples;
end
