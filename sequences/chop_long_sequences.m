function sequence_column_clone=chop_long_sequences(sequence_column,sequence_length_lower_bound,sequence_length_upper_bound,dist_object)



long_sequences = find_long_sequences(sequence_column,sequence_length_upper_bound);

sequence_column_clone = sequence_column;

for i=1:length(long_sequences)
    long_sequence = long_sequences(i);
    sequence_idxs = find(sequence_column==long_sequence);
    sequence_start = sequence_idxs(1);
    subsequence_lengths = [];

    while(true)
        new_seq_length = round(dist_object.random());
        if(new_seq_length>sequence_length_lower_bound)
            if(sum(subsequence_lengths)+new_seq_length>length(sequence_idxs))
                remainder_seq_length = length(sequence_idxs) - sum(subsequence_lengths);
                if(remainder_seq_length>sequence_length_lower_bound)
                    subsequence_lengths = [subsequence_lengths; remainder_seq_length];
                else
                    subsequence_lengths(end) = subsequence_lengths(end)+remainder_seq_length;
                end
                break;
            else
                subsequence_lengths = [subsequence_lengths; new_seq_length];
            end
        end
    end
    subsequence_lengths(subsequence_lengths==0) = [];
    subsequence_sub_idxs = cumsum(subsequence_lengths);
    subsequence_sub_idxs = subsequence_sub_idxs(1:end-1);

    for j = 1:length(subsequence_sub_idxs)
        session_suffix_idx = sequence_start+subsequence_sub_idxs(j);
        sequence_column_clone(session_suffix_idx:end) = sequence_column_clone(session_suffix_idx:end)+1;
    end
        
end