function new_sequence_column = defrag_sequence_column(sequence_column)

sequence_numbers = my_unique(sequence_column);
new_sequence_column = [];
if(~isempty(find(diff(sequence_column)>1)))
    for i = 1:length(sequence_numbers)
        seq_length = sum(sequence_column==sequence_numbers(i));
        new_seq_column_segment = repmat(i, seq_length,1);
        new_sequence_column = [new_sequence_column;new_seq_column_segment];
    end
end