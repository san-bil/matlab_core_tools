function [ frame_labels ] = sequence_labels_to_frame_labels( sequence_labels,sequence_column )
% takes a vector of sequence labels and a vector of sequence indices, and
% converts sequence labels to frame labels 
%
%e.g [0 1 0] and [1 1 1 1 2 2 3 3 3 3 3 3] ----> [0 0 0 0 1 1 0 0 0 0 0 0]
%   
%assert(length(sequence_labels)==length(my_unique(sequence_column)));

frame_labels = zeros(length(sequence_column),1);

sequence_lengths = get_sequence_lengths(sequence_column);

bookmark = 1;

for i = 1:length(sequence_lengths)
    frame_labels(bookmark:bookmark-1+sequence_lengths(i)) = ones(sequence_lengths(i),1)*sequence_labels(i);
    bookmark = bookmark+sequence_lengths(i);
end

%The "refactor" from the code below didn't really change the LOC...silly.

% sequence_numbers = my_unique(sequence_column);
% 
% for i = 1:size(sequence_numbers,1)
%     sequence_number = sequence_numbers(i);
%     sequence_idxs = sequence_column==sequence_number;
%     frame_labels(sequence_idxs) = ones(sum(sequence_idxs),1)*sequence_labels(i);
% end

end

