function [ sequence_number_column, sequence_counter ] = generate_dummy_sequence_numbers( ground_truth_column )
%GENERATE_DUMMY_SEQUENCE_NUMBERS 
%takes a sequence such as [0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1 1 0 0 0 1 1 1 1 1 0 0]
% and gives               [1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 2 2 3 3 3 4 4 4 4 4 5 5]
% used to build a column of sequence numbers for an unpresegmented entire session
sequence_number_column = zeros(size(ground_truth_column));

sequence_counter = 1;
sequence_number_column(1) = sequence_counter;

prev = ground_truth_column(1);

for i = 1:length(ground_truth_column)
    curr = ground_truth_column(i);
    
    if(curr==prev)
        
    else
        sequence_counter = sequence_counter+1;
    end
    
    sequence_number_column(i) = sequence_counter;
    prev=curr;
    
end

end

