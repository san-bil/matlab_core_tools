function [ sequence_column ] = create_sequence_column_from_ground_truth( ground_truth_column )

%given a ground truth column such as [0 0 0 1 1 1 0 0 0 1 1 1 1 0 0 1 1 1 1 1 1 0 0 0]
%this creates the sequence column    [1 1 1 2 2 2 3 3 3 4 4 4 4 5 5 6 6 6 6 6 6 7 7 7]


sequence_number = 1;
sequence_column =zeros(length(ground_truth_column),1);

for i = 1:length(ground_truth_column)
    curr = ground_truth_column(i);
    
    if(curr~=prev)
        sequence_number = sequence_number+1;
    end
    
    sequence_column(i) = sequence_number;
    
    prev = curr;
end

end

