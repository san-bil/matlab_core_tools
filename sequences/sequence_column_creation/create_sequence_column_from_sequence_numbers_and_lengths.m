function [ sequence_column ] = create_sequence_column_from_sequence_numbers_and_lengths( sequence_numbers,sequence_lengths )
%CREATE_SEQUENCE_COLUMN_FROM_SEQUENCE_NUMBERS_AND_LENGTHS Summary of this function goes here
%   Detailed explanation goes here
assert(length(sequence_numbers)==length(sequence_lengths))

sequence_column = zeros(sum(sequence_lengths),1);

marker = 1;
for i=1:length(sequence_numbers)

    sequence_column(marker:marker+sequence_lengths(i)-1) = repmat(sequence_numbers(i),sequence_lengths(i),1);
    marker = marker+sequence_lengths(i);
end

end

