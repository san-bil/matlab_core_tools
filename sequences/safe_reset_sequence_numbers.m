function [ resetted_sequence_column ] = safe_reset_sequence_numbers( sequence_column )

sequence_counter = 1;
resetted_sequence_column = zeros(size(sequence_column));

for i = 1:length(sequence_column)
    
    
    if(i>1 && (sequence_column(i)~=sequence_column(i-1)))
    
        sequence_counter=sequence_counter+1;
    
    end
    
    resetted_sequence_column(i) = sequence_counter;

end

