function [ sequence_lengths ] = safe_get_sequence_lengths( sequence_column )

sequence_lengths = [];

if(~isempty(sequence_column))

    sequence_length_counter = 1;
    for i = 1:length(sequence_column)
        if(i>1 && (sequence_column(i)==sequence_column(i-1)))
            sequence_length_counter=sequence_length_counter+1;
        elseif((i>1 && (sequence_column(i)~=sequence_column(i-1))))
            sequence_lengths = [sequence_lengths; sequence_length_counter];
            sequence_length_counter=1;
        end
        
        if(i==length(sequence_column))
            sequence_lengths = [sequence_lengths; sequence_length_counter];
        end
        
    end

else
    %do nothing.
end