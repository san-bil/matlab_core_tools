function [ output ] = agglomerate_sequence_labels( input,thresh )

output = input;
sequence_lengths = get_sequence_lengths(generate_dummy_sequence_numbers(input));

num_sequence_lengths_trunc = floor(length(sequence_lengths)/2)*2;
if(output(1)==1)
    sequence_activity_indicators=repmat([1;0],num_sequence_lengths_trunc/2,1);
elseif((output(1)==0))
    sequence_activity_indicators=repmat([0;1],num_sequence_lengths_trunc/2,1);
end

if(mod(length(sequence_lengths),2)>0)
    sequence_activity_indicators = [sequence_activity_indicators; ~sequence_activity_indicators(end)];
end

sequence_lengths_cumsum = cumsum(sequence_lengths);

for i = 1:length(sequence_lengths)
    if(sequence_activity_indicators(i)==0 && sequence_lengths(i)<=thresh && i>1)
        output(sequence_lengths_cumsum(i-1)+1:sequence_lengths_cumsum(i))=1;
    elseif(sequence_activity_indicators(i)==0 && sequence_lengths(i)<=thresh && i==1)
        output(1:sequence_lengths_cumsum(i))=1;
    end
end


end

