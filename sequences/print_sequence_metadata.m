function print_sequence_metadata(sequence,data_dict, frame_rate)


if(~exist('frame_rate','var'))
    frame_rate=58;
    warning('Assuming default frame rate of 58.')
end


key_partials = {'session',@unique,'';
                'subject',@unique,'';
                'sequence',@unique,'';
                'role',@unique,'';
                'frame',(@(x)[min(x) max(x)]),'';
                'frame',(@(x)[min(x)/frame_rate max(x)/frame_rate]),'(s)';
               };

for i=1:length(key_partials)
   
    key_match = kv_find(key_partials{i,1},data_dict);
    if(~isempty(key_match))
        idxs = kv_get(key_match,data_dict);
        cols = sequence(:,idxs);
        cols_processor = key_partials{i,2};
        processed_cols = cols_processor(cols);
        units = key_partials{i,3};
        disp([key_match ': ' num2str(processed_cols) units]);
    end
end