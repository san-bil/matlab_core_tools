function out=get_sequence_constant(col_key,data_dictionary,dataframe)

col_idx = kv_get(col_key,data_dictionary);
col = dataframe(:,col_idx);

assert(sum(diff(col))<0.0000000000000001);

out = col(1);

end
