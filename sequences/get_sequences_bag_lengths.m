function out = get_sequences_bag_lengths(in)

out = cellfun(@(x)size(x,1),in);