function [collapsed_sequences_ground_truth, collapsed_sequences_ids, collapsed_subjects_column]  = collapse_frames_to_sequences(label_column, sequence_column, subject_column)

collapsed_sequences_ids = my_unique(sequence_column);
collapsed_sequences_ground_truth = zeros(size(collapsed_sequences_ids,1),1);
collapsed_subjects_column = zeros(size(collapsed_sequences_ids,1),1);
           
for i = 1:length(collapsed_sequences_ids)
    sequence_idx = sequence_column == collapsed_sequences_ids(i);
    sequence_idx_numbers = find(sequence_idx);
    
    sequence_ground_truth = label_column(sequence_idx);
	collapsed_sequence_ground_truth = round(sum(sequence_ground_truth)/length(sequence_ground_truth));
    if(~sum(isnan(sequence_ground_truth))==length(sequence_ground_truth))
        assert(sum(sequence_ground_truth==collapsed_sequence_ground_truth)==length(sequence_ground_truth));
    end
    collapsed_sequences_ground_truth(i)=collapsed_sequence_ground_truth; 
    
    if(exist('subject_column','var'))
        subj = subject_column(sequence_idx_numbers(1));
        assert(length(unique(subject_column(sequence_idx)))==1 && unique(subject_column(sequence_idx))==subject_column(sequence_idx_numbers(1)));
        collapsed_subjects_column(i)=subj;
    end
end

end
