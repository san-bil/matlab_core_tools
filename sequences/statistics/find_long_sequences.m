function [long_sequences] = find_long_sequences(sequence_column, threshold_length)

sequence_numbers = my_unique(sequence_column);
sequence_lengths = get_sequence_lengths(sequence_column);

long_sequences = sequence_numbers(sequence_lengths>threshold_length);

