function [ class0_mean_length,class0_std_length,class1_mean_length,class1_std_length, kv_map] = calculate_average_run_length( binary_column )

%given a sequence of length N (ie a ground-truth vector), gives the average length of subsequences of each class

class0_run_lengths = []; 
class1_run_lengths = [];

counter=0;
prev = binary_column(1);

for i = 1:length(binary_column)
    
    curr = binary_column(i);
    
    if(curr~=prev)
        if(prev==0)
            class0_run_lengths = [class0_run_lengths counter];
        else
            class1_run_lengths = [class1_run_lengths counter];
        end
        counter = 0;
    end
    counter=counter+1;
    prev = curr;
end

class0_mean_length = mean(class0_run_lengths);
class1_mean_length = mean(class1_run_lengths);

class0_std_length = std(class0_run_lengths);
class1_std_length = std(class1_run_lengths);

kv_map = {'class0_mean_length',class0_mean_length;'class1_mean_length',class1_mean_length;'class0_std_length',class0_std_length;'class1_std_length',class1_std_length};

end

