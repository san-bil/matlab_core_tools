function [sequence_lengths] = get_sequence_lengths(sequence_column)


sequence_numbers = my_unique(sequence_column);
sequence_lengths = zeros(size(sequence_numbers));

for i = 1:length(sequence_numbers)

    sequence = sequence_column(sequence_column==sequence_numbers(i));
    sequence_lengths(i) = length(sequence);

end