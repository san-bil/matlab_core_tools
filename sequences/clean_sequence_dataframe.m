function clean_dataframe_chunk =  clean_sequence_dataframe(dataframe_chunk,exp_data_dictionary)

tmp_dirty_stacked_sequences = stacked_sequences_to_bag(dataframe_chunk,exp_data_dictionary);
clean_stacked_sequence_idxs = (cellfun(@(tmp_seq) ~isnan(sum(sum(tmp_seq))), tmp_dirty_stacked_sequences));
clean_stacked_sequences = tmp_dirty_stacked_sequences(clean_stacked_sequence_idxs);
clean_dataframe_chunk = bag_to_stacked_sequences(clean_stacked_sequences);
