function [out,matching_idxs]=filter_sequence_bag(filter_fhandle,sequence_bag)


matching_idxs_tmp = cellfun(filter_fhandle, sequence_bag);
matching_idxs = cell2mat(matching_idxs_tmp);
out = sequence_bag(matching_idxs);

end
