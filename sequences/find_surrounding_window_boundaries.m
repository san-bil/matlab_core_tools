function [idxs] = find_surrounding_window_boundaries(left,right,window_size,sequence_length, include_starting_window)

if((include_starting_window==1 && window_size<sequence_length) || (include_starting_window==0 && window_size+abs(right-left)<sequence_length))
    
    curr_left=left;
    curr_right=right;
    starting_window_size = right-left;

    while(1)
        curr_left = max(1, curr_left-1);
        curr_right=min(sequence_length, curr_right+1);

        if(include_starting_window && (curr_right-curr_left)>=window_size)
            idxs = curr_left:curr_right;
            break;
        elseif((curr_right-curr_left)>=window_size+starting_window_size)
            idxs = setdiff(curr_left:curr_right,left:right);
            break;
        end    
    end
elseif((include_starting_window==1 && window_size==sequence_length) || (include_starting_window==0 && window_size+abs(right-left)==sequence_length))
    idxs = 1:sequence_length;
else
    error('You can''t have a window bigger than your entire sequence! Jeez.')
end