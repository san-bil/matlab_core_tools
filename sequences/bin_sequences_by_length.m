function sequence_bins = bin_sequences_by_length(Xs, bin_upper_bounds)

expanded_bin_upper_bounds = [force_row_vec(bin_upper_bounds)];
sequence_lengths = cellfun(@(x)size(x,2), Xs);
sequence_bins = kv_create_lists(my_mat2cell(expanded_bin_upper_bounds));

for i = 1:length(Xs)
   
    sl = sequence_lengths(i);
    bin_idx = index_array(find(sl<expanded_bin_upper_bounds),1);
    upper_bound = expanded_bin_upper_bounds(bin_idx);
    sequence_bins = kv_append_val(upper_bound,Xs{i},sequence_bins);
end

