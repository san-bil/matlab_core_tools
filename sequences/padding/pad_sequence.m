function [ sequence ] = pad_sequence( sequence, pad_value, pad_length, pad_sequence_end )

%pads sequence with a given pad size and pad value. "pad_sequence_end"
%determines whether to pad the start or end of the sequence

if(~exist('sequence_end','var'))
    pad_sequence_end = 'end';
end
    
if(strcmp(pad_sequence_end,'end'))
    sequence=[sequence; repmat(pad_value,pad_length,1)];
else
    sequence=[repmat(pad_value,pad_length,1); sequence];
end


end

