function [ new_stream_1, new_stream_2] = lag_datastreams( stream_1, stream_2,lag )
%LAG_DATASTREAMS Summary of this function goes here
%   Detailed explanation goes here

    if(lag>0)
        new_stream_1 = stream_1(1:end-lag,:);
        new_stream_2 = stream_2(lag+1:end,:);
        
    elseif(lag<0)
        lag = abs(lag); %otherwise you index as end+lag below. which would be silly.
        new_stream_2 = stream_2(1:end-lag,:);
        new_stream_1 = stream_1(lag+1:end,:);
    else
        new_stream_1 = stream_1;
        new_stream_2 = stream_2;
    end



end

