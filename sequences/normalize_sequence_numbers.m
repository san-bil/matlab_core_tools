function normalized_sequence_column = normalize_sequence_numbers(sequence_column)



if(~isempty(find(diff(my_unique(sequence_column))>1)))
    normalized_sequence_column=zeros(size(sequence_column));
    curr_sequence_numbers = my_unique(sequence_column);
    new_sequence_numbers = 1:length(curr_sequence_numbers);

    for i = 1:length(my_unique(sequence_column))
        normalized_sequence_column(sequence_column==curr_sequence_numbers(i))=new_sequence_numbers(i);
    end
else
    normalized_sequence_column = sequence_column;
end