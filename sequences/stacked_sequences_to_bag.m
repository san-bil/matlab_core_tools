function bag = stacked_sequences_to_bag(stacked_sequences, data_dictionary)

sequence_column_idx = kv_get('sequence_idx',data_dictionary);
sequence_column = stacked_sequences(:,sequence_column_idx);
sequence_numbers = my_unique(sequence_column);
bag = cell(length(sequence_numbers),1);

for i = 1:length(sequence_numbers)
    sequence = stacked_sequences(sequence_column==sequence_numbers(i),:);
    bag{i} = sequence;
end
