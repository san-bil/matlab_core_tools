function to_add=add_fliplr_images(data, unfolder,refolder)

to_add = zeros(size(data));

for i=1:size(to_add,1)
   
    to_add(i,:)=refolder(fliplr(unfolder(data(i,:))));
    
end

