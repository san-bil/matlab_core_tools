function [data,state_file, D]=convert_images_to_data_array(raw_data_folder,func_pipeline,file_prefix,sample_size) 

if(~exist('file_prefix','var'))
    file_prefix = '';
end

state_file = [raw_data_folder filesep file_prefix 'data.mat'];

if(~exist(state_file,'file'))
    files = dirrec(raw_data_folder,'.jpg');
    
    if(exist('sample_size','var'))
        files_subsample = files(randsample(length(files),sample_size));
    else
        files_subsample = files;
    end
    
    
    
    trial_vector = imread(files_subsample{1});
    for k = 1:length(func_pipeline)    
            func_h = func_pipeline{k};
            trial_vector=func_h(trial_vector);
    end
    
    D=length(trial_vector);
    
    data = zeros(length(files_subsample),D);
    
    for i = 1:length(files_subsample)
        file_name = files_subsample{i};
        my_image = imread(file_name);
        data_vector=my_image;
        for j = 1:length(func_pipeline)
        
            func_h = func_pipeline{j};
            data_vector=func_h(data_vector);
            

        end
        
        data(i,:)=single(data_vector);
        
        if(mod(i,100)==0)
                disp(['image conversion up to: ' num2str(i)]);
        end
    end
    save(state_file,'data','-v7.3');
else
    load(state_file)
end

% func_pipeline = {
%     @(tmp_im_1) imresize(int_im_1,[30 40]);
%     @(tmp_im_2) rgb2gray(tmp_im_2);
%     @(tmp_im_3) reshape(tmp_im_3',[(numel(tmp_im_3)),1])';
% };
% 
% convert_images_to_data_array(raw_data_folder,1200,func_pipeline) 

