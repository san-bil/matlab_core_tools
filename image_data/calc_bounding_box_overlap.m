function perc_overlap = calc_bounding_box_overlap(box_1, box_2)

%bbox args are in the form [topleft_dwn_idx, topleft_across_idx, height, width]

box1_topleft = [box_1(1),box_1(2)];
box1_topright = [box_1(1),box_1(2)+box_1(4)];
box1_botleft = [box_1(1)+box_1(3),box_1(2)];
box1_botright = [box_1(1)+box_1(3),box_1(2)+box_1(4)];
box1_size = box_1(3)*box_1(4);

box2_topleft = [box_2(1),box_2(2)];
box2_topright = [box_2(1),box_2(2)+box_2(4)];
box2_botleft = [box_2(1)+box_2(3),box_2(2)];
box2_botright = [box_2(1)+box_2(3),box_2(2)+box_2(4)];
box2_size = box_2(3)*box_2(4);
 
intersection_left = max(box1_topleft(2), box2_topleft(2));
intersection_right = min(box1_topright(2), box2_topright(2));
intersection_bottom = min(box1_botleft(1), box2_botleft(1));
intersection_top = max(box1_topleft(1), box2_topleft(1));

area = (intersection_bottom-intersection_top)*(intersection_right-intersection_left);

perc_overlap=area/box1_size;



end

%{     

         test_box_1 = [106,138,228,228];
         test_box_2 = [106,138,228,228];
         calc_bounding_box_overlap(test_box_1, test_box_2)

%}     