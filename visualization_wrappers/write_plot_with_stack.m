function write_plot_with_stack(fig_generator, plot_path, options)

if(strcmp(class(fig_generator),'function_handle'))
    fig_handle=fig_generator();
elseif(strcmp(class(fig_generator),'matlab.ui.Figure'))
    fig_handle=fig_generator;
end
figure(fig_handle);

plot_format = kv_get('plot_format',options,'jpeg');
height_inches = kv_get('height_inches',options,8);
width_inches = kv_get('width_inches',options,13);


hgexport(fig_handle, plot_path, hgexport('factorystyle'), 'Format', plot_format, 'Resolution', 300, 'Width', width_inches, 'Height', height_inches, 'Units', 'inch');
close(fig_handle);

[plot_file_parent, plot_file_name]=get_parent_dir(plot_path);
[plot_file_stem]=split_filename(plot_file_name);
stack_file_name=[plot_file_stem '_stack.txt'];
stack_file_path = [plot_file_parent filesep stack_file_name];

write_stack_to_file(stack_file_path,add_to_stackignore(options));
