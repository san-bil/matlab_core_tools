function plot_sequence_bag(sequence_bag, pane_width)

if(~exist('pane_width','var'))
    pane_width = 5;
end

figure;

pane_depth = ceil(length(sequence_bag)/pane_width);

for i = 1 : length(sequence_bag)
   
    subplot(pane_depth,pane_width,i);
    plot(sequence_bag{i}');
    
end


end