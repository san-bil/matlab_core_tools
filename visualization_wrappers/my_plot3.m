function fig_handle = my_plot3(X, fig_handle)

if(exist('fig_handle','var'))
    figure(fig_handle);
    hold on;
else
    fig_handle = figure;
end

if(size(X,2)>size(X,1))
    X = X';
end

plot3(X(:,1),X(:,2),X(:,3));
