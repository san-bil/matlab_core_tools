function [normalized_col] = normalize_pose_energy( pose, col_idx)

%relevant_cols = setdiff(1:size(pose,2),col_idx);
relevant_cols = 1:size(pose,2);
normalizer = sum(abs(pose(:,relevant_cols)),2); 

col = pose(:,col_idx);

normalized_col = col./normalizer;

end

