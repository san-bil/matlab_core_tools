function Xsn_out = interp_shorter_sequence(Xsn)

shorter_idx = argmin(cellfun(@(tmp)size(tmp,2), Xsn));
longer_idx = setdiff(1:2,shorter_idx);
long_seq = Xsn{longer_idx}';
short_seq = Xsn{shorter_idx}';

step_size = length(short_seq)/length(long_seq);

my_interper = @(tmp) interp1(1:length(tmp), tmp, 1:step_size:length(tmp))';
res = column_apply(short_seq, my_interper, 0);

length_diff = abs(length(long_seq)-length(res));
end_window_mean = mean(res(end-100:end,:));

res = [res; repmat(end_window_mean, length_diff,1) ];



Xsn_out = Xsn;
Xsn_out{shorter_idx}=res';