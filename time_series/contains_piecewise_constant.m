function [res,masked_sig]=contains_piecewise_constant(X0, temporal_resolution)

sig_diff = diff(X0,1,2);
mask = ones(1,temporal_resolution);

constant_sig = +(sig_diff == 0);

masked_sig = conv2(constant_sig,mask);

if(nd_sum(masked_sig>=temporal_resolution)>0)
    res=1;
else
    res=0;
end