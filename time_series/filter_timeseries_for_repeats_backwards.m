function [clean_values,clean_timestamps, clean_idxs, dirty_idxs] = filter_timeseries_for_repeats_backwards(timestamps,values)

values = force_skinny_matrix(values);
timestamps = (force_skinny_matrix(timestamps));

clean_idxs = [];
dirty_idxs = [];
for i = 1:length(timestamps)
    ts = (timestamps(i));
    gthan = timestamps(i+1:end)>ts;
    if(sum(gthan)==length(gthan))
        clean_idxs = [clean_idxs; i];
    else
        dirty_idxs = [dirty_idxs; i];

    end
end

clean_values = values((clean_idxs),:);
clean_timestamps = timestamps((clean_idxs),:);
tmp=1;