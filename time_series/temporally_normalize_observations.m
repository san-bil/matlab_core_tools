function [vq, xq] = temporally_normalize_observations(Xs, timestamps, fr)

% frame_stamps_normalized = 0:1:(max(timestamps)*fr);
% Xs_normalized = zeros(length(frame_stamps_normalized),size(Xs,2))
% for i = 1:length(timestamps)
% 
%     frame_stamp = timestamps(i)*fr;
%     diffs = abs(frame_stamp - frame_stamps_normalized);
%     [~,min_diff_idx]=min(diffs);
%     frame_stamps_normalized(i) = 
%     
% end
framestamps = timestamps*fr;
xq = 0:1:(max(framestamps));
vq = interp1(framestamps,Xs,xq);