function [center_points, bucketed_data]  = bucket_time_series(timestamps, data, temporal_bucket_length, bucketing_method)

[center_points, bucket_idxs] = snap_timestamps(timestamps, temporal_bucket_length);
bucketed_data = zeros(length(center_points),size(data,2));

buckets_ids = my_unique(bucket_idxs);

for i=1:length(buckets_ids)
   bucket_id = buckets_ids(i);
   bucket_scope = bucket_idxs==bucket_id;
   bucket_data = data(bucket_scope,:);
   combined_bucket_data = bucketing_method(bucket_data);
   bucketed_data(i,:) = combined_bucket_data;
end
