function [center_points, bucket_idxs] = snap_timestamps(timestamps, temporal_bucket_length)

center_points = 0:temporal_bucket_length:ceil(max(timestamps));
bucket_idxs = zeros(size(timestamps));

identifiability_offset = (temporal_bucket_length/1000);

for i = 1:length(timestamps)
    timestamp = timestamps(i);
    offset_timestamp = timestamp+identifiability_offset;
    time_diffs = abs(center_points - offset_timestamp);
    candidates = time_diffs <=(temporal_bucket_length/2);
    match = find(candidates, 1);
    bucket_idxs(i) = match; 
end