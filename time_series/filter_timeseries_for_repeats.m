function [clean_values,clean_timestamps, clean_idxs, dirty_idxs] = filter_timeseries_for_repeats(timestamps,values)

values = force_skinny_matrix(values);
timestamps = (force_skinny_matrix(timestamps));

clean_idxs = ones(size(timestamps));
dirty = [];
for i = 1:length(timestamps)
    ts = (timestamps(i));
    gthan = timestamps(i+1:end)>=ts;
    clean_idxs(i+1:end) = clean_idxs(i+1:end) & gthan;
end

dirty_idxs = find(~clean_idxs);
clean_values = values(logical(clean_idxs),:);
clean_timestamps = timestamps(logical(clean_idxs),:);
tmp=1;