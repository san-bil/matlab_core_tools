function [ data_matrix ] = cut_up_univariate_time_series_into_vectors( time_series_data, window_size )

% in: a numeric vector (represents a univariate time series)
%
% out: a matrix, where each row corresponds to a sliding window over the vector
%
% example: cut_up_univariate_time_series_into_vectors([1 2 3 4 5 6],3) would give:
%
%           [1 2 3]
%           [2 3 4]
%           [3 4 5]
%           [4 5 6]
%
% desc: as above
%
% tags: #slidingwindow #window #timeseries #sequence

data_matrix = zeros(size(time_series_data,1)-window_size,window_size+1);

marker_1 = 1;
marker_2 = window_size;

while(true)

    data_matrix(marker_1,1:window_size) = time_series_data(marker_1:marker_2); %predictors
    data_matrix(marker_1,window_size+1) = time_series_data(marker_2+1);%target
    marker_1=marker_1+1;
    marker_2=marker_2+1;
    
    if(marker_1>size(data_matrix,1))
           break;
    end
    
end

end

