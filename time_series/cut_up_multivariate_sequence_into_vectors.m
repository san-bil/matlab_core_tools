function [ data_matrix,target_matrix ] = cut_up_multivariate_sequence_into_vectors( time_series_data, window_size)

% in: a numeric matrix (represents a multivariate time series)
%
% out: a matrix, where each row corresponds to a sliding window over the initial matrix
%
% example: cut_up_multivariate_time_series_into_vectors([1 2 3 4 5 6],3) would give:
%
%           [1 1 1]                                     [1 1 1 2 2 2]
%       (   [2 2 2]   ,   2  )  ....would give....      [2 2 2 3 3 3]
%           [3 3 3]                                     [3 3 3 4 4 4]
%           [5 5 5]                                     [4 4 4 5 5 5]
%           [6 6 6]                                     [5 5 5 6 6 6]
%           [7 7 7]                                     [6 6 6 7 7 7]
%                                                       

% desc: as above
%
% tags: #slidingwindow #window #timeseries #sequence

num_dims = size(time_series_data,2);

data_matrix = zeros(size(time_series_data,1)-window_size,(window_size)*num_dims);
target_matrix = zeros(size(time_series_data,1)-window_size,num_dims);

marker_1 = 1;
marker_2 = window_size;

while(true)

    data_matrix(marker_1,1:window_size*num_dims) = reshape(time_series_data(marker_1:marker_2,:)',1,num_dims*window_size); %predictors
    
    target_matrix(marker_1,:) = time_series_data(marker_2+1,:);

    
    marker_1=marker_1+1;
    marker_2=marker_2+1;
    
    if(marker_1>size(data_matrix,1))
           break;
    end
    
end

end

