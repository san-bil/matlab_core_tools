function [ output ] = rowfun( X, fhandle, output_style )
%ROWFUN Summary of this function goes here
%   Detailed explanation goes here

if(strcmp(output_style,'numeric'))
    output = [];
elseif(strcmp(output_style,'cell'))
    output = {};
else
    error('invalid output type')
end

for i = 1:size(X,1)

    row_res = fhandle(X(i,:));
    if(strcmp(output_style,'numeric'))
        output = [output; row_res];
    elseif(strcmp(output_style,'cell'))
        output{end+1} = {row_res};
    else
        error('invalid output type')
    end
    
    
end


end

