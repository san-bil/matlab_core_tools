function [output,centrepoints] = sliding_window(input_signal,func_handle,input_window_size,stride,crop_or_decay_edges)

% in: a uni/multi variate signal
%     handle of function to apply to each window
%     window size
%     the overlap between successive windows
% out: signal, having had the function applied to each window
%
% desc: slides a window across a signal, and returns output signal
%
% tags: #windows #slidingwindow #sliding #filtering



input_num_windows=length(input_signal)/stride;
centrepoints = zeros(input_num_windows,1);
output_window_size = size(func_handle(input_signal(1:input_window_size)));

output = zeros(input_num_windows*output_window_size(1),output_window_size(2));

i=0;
while(true)

        input_window_start = (i*stride)+1;
        input_window_end = input_window_start+input_window_size-1;
        
        output_window_start = (i*stride)+1;
        output_window_end = output_window_start+output_window_size(1);
        
        if(input_window_end>length(input_signal))
            break;
        end
        
        input_window = input_signal(input_window_start:input_window_end,:);
        
        output(output_window_start:output_window_end) = func_handle(input_window);
        centrepoints(i+1) = (input_window_start+input_window_end)/2;
        i=i+1;
end