function res = column_apply(input, func_handle, res_into_cell)

if(~exist('res_into_cell','var'))
    res_into_cell=0;
end


if(res_into_cell)

    res = {};
    for i=1:size(input,2)

        res{end+1} = func_handle(input(:,i));

    end

else

    res = [];
    for i=1:size(input,2)

        res = [res func_handle(input(:,i))];

    end

end