figure;
handles = {@barthannwin, @bartlett, @blackman, @blackmanharris, @bohmanwin, @chebwin, @flattopwin, @hann, @hamming, @kaiser, @nuttallwin, @parzenwin, @rectwin, @taylorwin, @triang, @tukeywin};


for i = 1:length(handles)
    handle = handles{i};
    plot(handle(50));
    hold all;
    disp(func2str(handle));
    pause
end