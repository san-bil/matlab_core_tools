function [output,centrepoints] = window_apply(input,func_handle,win_size,overlap)

% in: a uni/multi variate signal
%     handle of function to apply to each window
%     window size
%     the overlap between successive windows
% out: signal, having had the function applied to each window
%
% desc: slides a window across a signal, and returns output signal
%
% tags: #windows #slidingwindow #sliding #filtering


num_windows = floor((size(input,1)/win_size));
output = [];
centrepoints = [];
window_start_acc = [];
window_end_acc = []; 
length_acc = [];
for i = 1:num_windows

    for j = 1:overlap
    
        window_start = (i-1)*win_size+j;
        window_end = window_start+win_size-1;
        
        window_start_acc = [window_start_acc;window_start];
        window_end_acc = [window_end_acc;window_end];
        try
            window=input(window_start:window_end,:);
        catch error
            break
        end
        output = [output; func_handle(window)];
        length_acc = [length_acc; length(output)];
        centrepoints = [centrepoints; mean([window_start, window_end])];
        
    end
    
end