function [ out_tensor ] = tensor_z_apply( tensor,func_handle )

target_length = length(func_handle(permute(tensor(1,1,:),[3,1,2])));

out_tensor = zeros(size(tensor,1),size(tensor,2),target_length);

for i=1:size(tensor,1)
    for j=1:size(tensor,2)
        vec=tensor(i,j,:);

        rotated_vec = permute(vec,[3,1,2]);
        processed_vec = func_handle(rotated_vec);
        out_tensor(i,j,:)=processed_vec;

    end
end

