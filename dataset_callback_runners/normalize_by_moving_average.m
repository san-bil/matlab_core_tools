function [output] = normalize_by_moving_average(input_signal,input_window_size, windower)

% in: a uni/multi variate signal
%     handle of function to apply to each window
%     window size
%     the overlap between successive windows
% out: signal, having had the function applied to each window
%
% desc: slides a window across a signal, and returns output signal
%
% tags: #windows #slidingwindow #sliding #filtering

input_signal = force_skinny_matrix(input_signal);

i=1;
window_relative_idxs = (1:input_window_size)-round(input_window_size/2);
output = [];
window_unshifted_idxs = 1:input_window_size;
while(true)

        window_centre = i;
        window_absolute_idxs = window_relative_idxs+window_centre;
        
        window_absolute_idx_clean = window_absolute_idxs(window_absolute_idxs>0 & window_absolute_idxs<=length(input_signal));
        window_relative_idx_clean = window_unshifted_idxs(window_absolute_idxs>0 & window_absolute_idxs<=length(input_signal));
        
        window_weights = windower(input_window_size);
        
        window_weights_clean = window_weights(window_relative_idx_clean);
        input_window_weights_clean_integral = sum(window_weights_clean);
        
        input_window = input_signal(window_absolute_idx_clean,:);
        
        weighted_input_window = input_window.*repmat(window_weights_clean,1,size(input_signal,2));
        filtered_weighted_input_window = nan_inf_filter(weighted_input_window);
        window_mean = sum(filtered_weighted_input_window)/input_window_weights_clean_integral;
        
        window_variance = (sum(filtered_weighted_input_window.^2)/input_window_weights_clean_integral)-(window_mean*window_mean);
        window_stdev = sqrt(window_variance);
        window_iqr = iqr(filtered_weighted_input_window);
        
        output = [output; ((input_signal(i)-window_mean))];
        i=i+1;
        if(i>length(input_signal))
            break;
        end
end