function [performance_metrics,risk_adjusted_confusion_matrix, wrapper]=generate_metrics(predictions_column, ground_truth_column, cost_matrix)

%presume inputs are column vectors
            
            if(nargin<3)
                cost_matrix = [1 1; 1 1];
            end
            
            predictions_column=double(predictions_column);
            ground_truth_column=double(ground_truth_column);
            
            %confusion matrix:
            %       __________prediction___________
            %actual|_______tn_____|_______fp_______|
            %      |_______fn_____|_______tp_______|
            
            
            confusion_matrix = confusionmat(ground_truth_column,predictions_column);
            
            risk_adjusted_confusion_matrix = confusion_matrix .* cost_matrix;
            
            tn = risk_adjusted_confusion_matrix(1,1);
            tp = risk_adjusted_confusion_matrix(2,2);
            fn = risk_adjusted_confusion_matrix(2,1);
            fp = risk_adjusted_confusion_matrix(1,2);
            
            accuracy = (tp+tn)/(tp+tn+fp+fn);
            
            npv = tn/(tn+fn);
            specificity = tn/(fp+tn);
            class0f1 = 2*(npv*specificity)/(npv+specificity);            
            precision = tp/(tp+fp);
            recall = tp/(tp+fn);
            f1 = 2*(precision*recall)/(precision+recall);
            
            fpr = fp/(tn+fp);
            tpr = tp/(fn+tp);

            
            performance_metrics = {[accuracy precision recall f1], confusion_matrix};
            wrapper = kv_create(accuracy,precision,recall,f1,npv,specificity,fpr,tpr,confusion_matrix);
            
end

