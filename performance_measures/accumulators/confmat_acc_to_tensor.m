function [ conf_mat_tensor ] = confmat_acc_to_tensor( cell_of_conf_mats )

tmp = cell2mat(cell_of_conf_mats);
conf_mat_shape = size(cell_of_conf_mats{1});
conf_mat_tensor = reshape(tmp,conf_mat_shape(1),conf_mat_shape(2),numel(tmp)/prod(conf_mat_shape));

end

