 
%pretty self explanatory...generates performance metrics: accuracy,
%precision, recall, f1 measure, and an internal confusion matrix, which
%could be output if you reeeally need it.


function [returnVal,risk_adjusted_confusionMatrix]=generate_unblended_metrics(predictions_column, ground_truth_column, cost_matrix)

%presume inputs are column vectors
            
            if(nargin<3)
                cost_matrix = [1 1; 1 1];
            end
            
            %confusion matrix:
            %       __________prediction___________
            %actual|_______tn_____|_______fp_______|
            %      |_______fn_____|_______tp_______|
            
            
           
            confusion_matrix = confusionmat(ground_truth_column, predictions_column);
            
            risk_adjusted_confusionMatrix = confusion_matrix; %.* cost_matrix;
            
            tn = risk_adjusted_confusionMatrix(1,1);
            tp = risk_adjusted_confusionMatrix(2,2);
            fn = risk_adjusted_confusionMatrix(2,1);
            fp = risk_adjusted_confusionMatrix(1,2);
            
            accuracy1 = (tp+tn)/(tp+tn+fp+fn);
            
            class0Precision = tn/(tn+fn);
            class0Recall = tn/(fp+tn);
            class0f1 = 2*(class0Precision*class0Recall)/(class0Precision+class0Recall);
            
            class1Precision = tp/(tp+fp);
            class1Recall = tp/(tp+fn);
            class1f1 = 2*(class1Precision*class1Recall)/(class1Precision+class1Recall);
            
            %so do I weight them based on relative instance counts in the
            %test dataset or what? or just 0.5?
            
%            returnVal = {accuracy1; {'class0';'precision',class0Precision;'recall',class0Recall;'f1',class0f1};{'class1';'precision',class1Precision;'recall',class1Recall;'f1',class1f1}, confusionMatrix};
 
            returnVal = {[accuracy1, class0Precision,class0Recall,class0f1,class1Precision,class1Recall,class1f1], confusion_matrix};

end

