function [sequences_predictions, sequences_ground_truth, sequences_ids, collapsed_blocks_column]  = get_sequence_predictions( predictions, ground_truth_column, sequences_column, collapse_method, block_column)

if(~exist('collapse_method','var'))
    majority_voter = @(c) round(sum(c)/length(c));
    collapse_method=majority_voter;
end

sequences_ids = my_unique(sequences_column);
sequences_predictions = zeros(size(sequences_ids,1),1);
sequences_ground_truth = zeros(size(sequences_ids,1),1);
collapsed_blocks_column = zeros(size(sequences_ids,1),1);
           
for i = 1:length(sequences_ids)
    sequence_idx = sequences_column==sequences_ids(i);
    sequence_frames = find(sequence_idx);
    
    sequence_predictions = predictions(sequence_idx);
    sequence_ground_truth = ground_truth_column(sequence_idx);
    
    sequence_predicted_label = collapse_method(sequence_predictions);
    sequence_actual_label = collapse_method(sequence_ground_truth);

    assert(sum(sequence_ground_truth==sequence_actual_label)==length(sequence_ground_truth));
    assert(length(unique(block_column(sequence_idx)))==1)
    block_id = block_column(sequence_frames(1));
    assert(unique(block_column(sequence_idx))==block_id);
    
    collapsed_blocks_column(i)=block_id;
    sequences_predictions(i)=sequence_predicted_label;
    sequences_ground_truth(i)=sequence_actual_label; 
end

end

%{
sample input

s = [1,1,1,1,1,2,2,2,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4].*2;
d = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2];
t = [1,1,1,0,1,0,0,0,1,1,1,1,0,1,0,1,1,1,0,0,0,1,1,1];
z = [1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
s=s';
t=t';
z=z';

[predictedSequenceLabels actualSequenceLabels sequenceIDs processedSubjColumn] =  majorityVotingPredictionProcessingSequenceWise(t,z,s,d);

-----------------------------------------------------
s=[]
for i = 1:5
s = [s;(ones(10,1).*i)]
end

t=[]
for j = 1:5
t = [t;(rand(10,1)<(j*0.2))]
end

%}