function [true_pos_curve, false_pos_curve] = get_positive_predictions_curves(confusion_mat_tensor)

false_pos_curve = permute(confusion_mat_tensor(1,2,:)/(confusion_mat_tensor(1,1,:)+confusion_mat_tensor(1,2,:)), [3 2 1]);
true_pos_curve = permute(confusion_mat_tensor(2,2,:)/(confusion_mat_tensor(2,1,:)+confusion_mat_tensor(2,2,:)), [3 2 1]);