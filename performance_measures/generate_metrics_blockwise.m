function block_results = generate_metrics_blockwise(predictions, ground_truth_column, block_id_column )

    block_ids = my_unique(block_id_column);
    block_results = cell(length(block_ids),2);

    for i = 1:length(block_ids)

        block_id = block_ids(i);

        block_predictions = predictions(block_id_column==block_id);
        block_ground_truth = ground_truth_column(block_id_column==block_id);
        [~,~,res] = generate_metrics(block_predictions, block_ground_truth);

        block_results{i,1} = block_id;
        block_results{i,2} = res;
    end

end