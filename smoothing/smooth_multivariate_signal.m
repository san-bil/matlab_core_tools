function smoothed_data = smooth_multivariate_signal(data,params)

% in: a multivariate signal (num_samples x dimensionality)
%
% out: smoothed signal, same dimensionality as input
%
% desc: the multivariate signal is smoothed using a savitzky-golay filter (i.e. fitting
% a polynomial to a sliding window), with separate parameters for each signal component,
% as some may be more/less noisy, and hence need more more/less aggressive smoothing parameters.
%
% example usage:
%   data = Nx4 matrix
%   params = { 
%              4,57;
%              4,57;
%              4,57;
%              -1,-1; <-- so the last column won't be smoothed. the prior 3
%            }            will be smoothed using a quartic over a 57 sample window
%            
%   smooth_multivariate_signal(data,params)
%
% tags: #smoothing #filtering #lowpass #signal #filter

smoothed_data = zeros(size(data));

poly_degree_idx = 1;
window_size_idx = 2;

parfor i = 1:size(data,2)

    uni_signal = data(:,i);
    poly_degree = params{i,poly_degree_idx}; 
    window_size = params{i,window_size_idx};
    
    if(poly_degree==-1 && window_size==-1)
        smoothed_uni_signal = uni_signal;
    else
	    smoothed_uni_signal = sgolayfilt(uni_signal, poly_degree, window_size);
    end
    
    smoothed_data(:,i) = smoothed_uni_signal;
end




