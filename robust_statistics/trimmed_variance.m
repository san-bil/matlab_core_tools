function [trimmed_var,relevant_idxs] = trimmed_variance(in, percent)

thresh=percent/100;


lower_bound = quantile(in,thresh);
upper_bound = quantile(in,1-thresh);

relevant_idxs = ~(in<lower_bound | upper_bound<in);

trimmed_var = var(in(relevant_idxs));