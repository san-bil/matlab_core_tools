function new_data_dict = subsume_data_dictionary(subsuming_cols_name, parent_data_dict, child_data_dict)

features_idx = kv_get(subsuming_cols_name,parent_data_dict);
offset_child_data_dict = offset_data_dictionary(features_idx(1),child_data_dict);
new_data_dict = kv_join(parent_data_dict,offset_child_data_dict);