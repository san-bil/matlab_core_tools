function res = data_dictionary_equals(data_dictionary_1, data_dictionary_2)

if((size(data_dictionary_1,1)==size(data_dictionary_2,1)) && size(data_dictionary_1,1)>0)

    keys_1=kv_getkeys(data_dictionary_1);
    keys_2=kv_getkeys(data_dictionary_2);


    res=0;

    if(length(keys_1)~=length(keys_2))
        return;
    end


    for i = 1:length(keys_1)

        key_1 = keys_1{i};
        val_1 = kv_get(key_1,data_dictionary_1);
        val_2 = kv_get(key_1,data_dictionary_2);

        if(~ismember(key_1,keys_2) || length(val_1)~=length(val_2) || sum(sort(val_1)==sort(val_2))~=length(val_1));
            return;
        end


    end

    res=1;
else
    warning('data_dictionary_equals: one of the dictionaries is empty');
end