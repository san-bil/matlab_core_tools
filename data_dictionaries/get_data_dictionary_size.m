function dict_size = get_data_dictionary_size(data_dictionary)

dict_size = max(max(cell2mat(data_dictionary(:,2))));