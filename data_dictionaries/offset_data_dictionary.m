function offset_dd = offset_data_dictionary(offset,data_dictionary)

offset=offset-1;
offset_dd=data_dictionary;

offset_dd(:,2) = cellfun(@(x)x+offset,offset_dd(:,2),'UniformOutput',0);