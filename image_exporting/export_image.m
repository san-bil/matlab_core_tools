function export_image( rez, full_file_path, format )

% in: desired resolution, file_path of output image, file format extension 
%
% out: nada.
%
% desc: takes current figure (that which is returned by gcf), and exports it to an image at the desired resolution. I use it for pdfs.
%
% tags: #image #images #export

%rez=1200; %resolution (dpi) of final graphic
f=gcf; %f is the handle of the figure you want to export

figpos=getpixelposition(f); %dont need to change anything here
resolution=get(0,'ScreenPixelsPerInch'); %dont need to change anything here

set(f,'paperunits','inches','papersize',figpos(3:4)/resolution,'paperposition',[0 0 figpos(3:4)/resolution]);


print(f,full_file_path,['-d' format],['-r',num2str(rez)],'-opengl') %save file 

end

