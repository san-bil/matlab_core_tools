function media_file = get_eaf_video(eaf_file)


text = fileread(eaf_file);
lines = regexp(text,'\n','split');

for i = 1:length(lines)

    line = lines{i};
    if(~isempty(regexp(line,'MEDIA_URL')))
        
        [start_idx,end_idx] = regexp(line,'file[^\"]*"');
        media_file = line(start_idx+8:end_idx-1);
        break;
        
        
    end

end

if(~strcmp(media_file(1),'/'))
   
    media_file = ['/' media_file];
    
end