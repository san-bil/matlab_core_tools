function [annotation_matrices,structure_data_dictionary] = create_annotation_matrices_from_ELAN_batch(file, file_importer, frame_rate)       

batch_elan_annotations = file_importer(file);

event_set = unique(batch_elan_annotations(:,'behaviour'));
event_idxr = @(event_name) find(strcmp(table2array(event_name),table2array(event_set)));
eaf_file_list = unique(batch_elan_annotations(:,'src_eaf'));

annotation_matrices = {};

for j = 1:size(eaf_file_list,1)
    eaf_filename = cell2mat(table2array(eaf_file_list(j,'src_eaf')));
    
    annotation_list = batch_elan_annotations(strcmp(table2array(batch_elan_annotations(:,'src_eaf')),eaf_filename),:);
    
    video_file_path = get_eaf_video(eaf_filename);

    video_length = get_video_length(video_file_path,'ffprobe');
    annotation_matrix = zeros(video_length,size(event_set,1));

    for k = 1:height(annotation_list)    
        event_name = annotation_list(k,'behaviour');
        annotation_matrix_behaviour_idx = event_idxr(event_name);

        start_time = table2array(annotation_list(k,'start_short'));
        end_time = table2array(annotation_list(k,'end_short'));
        
        start_frame = round(clip_array_index(start_time*frame_rate,1,video_length));
        end_frame = round(clip_array_index(end_time*frame_rate,1,video_length));

        annotation_matrix(start_frame:end_frame,annotation_matrix_behaviour_idx) = 1;
    end
    
    data_dictionary = [cellfun(@sanitize_string, table2array(event_set),'UniformOutput',0) my_mat2cell((1:height(event_set))')];
    data_dictionary = kv_suffix_all_keys('_idx',data_dictionary);
    annotation_matrices(j,:) = {eaf_filename, video_file_path, data_dictionary, annotation_matrix};
end

structure_data_dictionary=kv_create_w_names('eaf_idx',1,'video_idx',2,'annotations_data_dictionary',3,'annotation_matrix',4);