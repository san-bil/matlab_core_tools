function [annotation_matrices] = create_annotation_matrix_from_ELAN(file, file_importer, frame_rate, annotation_file_list, options)       

elan_annotations = file_importer(file);
event_set = unique(elan_annotations(:,'behaviour'));
event_idxr = @(event_name) find(strcmp(table2array(event_name),table2array(event_set)));
eaf_file_list = unique(elan_annotations(:,'src_eaf'));
annotation_matrices = {};

for j = 1:size(eaf_file_list,1)
    
    eaf_file_path = platform_path_rewriter(cell2mat(table2array(eaf_file_list(j,'src_eaf'))));
    eaf_file_name = basename( eaf_file_path );
    path_prefix = kv_get('path_prefix',options,'');
    eaf_file_path =  prepend_path(path_prefix, eaf_file_path);
    
    if((~exist('annotation_file_list','var')) || ( iscell(annotation_file_list) && ismember(eaf_file_name,annotation_file_list)) || (ischar(annotation_file_list) && strcmp(annotation_file_list,'all')) )
        annotation_list = elan_annotations(strcmp(cellfun(@basename,table2array(elan_annotations(:,'src_eaf')),'UniformOutput',0),eaf_file_name),:);
        
        video_file_path = get_eaf_video(eaf_file_path);
        video_file_path = prepend_path(path_prefix,video_file_path);
        
        video_length = get_video_length(video_file_path,'text');
        annotation_matrix = zeros(video_length,size(event_set,1));

        for k = 1:height(annotation_list)    
            event_name = annotation_list(k,'behaviour');
            annotation_matrix_behaviour_idx = event_idxr(event_name);
            start_time = table2array(annotation_list(k,'start_short'));
            end_time = table2array(annotation_list(k,'end_short'));
            start_frame = round(clip_array_index(start_time*frame_rate,1,video_length));
            end_frame = round(clip_array_index(end_time*frame_rate,1,video_length));
            annotation_matrix(start_frame:end_frame,annotation_matrix_behaviour_idx) = 1;
        end

        data_dictionary = [cellfun(@sanitize_string, table2array(event_set),'UniformOutput',0) my_mat2cell((1:height(event_set))')];
        data_dictionary = kv_suffix_all_keys('_idx',data_dictionary);
        annotation_matrices{end+1} = kv_create(eaf_file_path, video_file_path, data_dictionary, annotation_matrix);
        
    end
end
tmp=1;


