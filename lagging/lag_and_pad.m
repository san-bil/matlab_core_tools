%You have two streams of data, stream 1 and stream 2:
%
%THE LAG IS DEFINED RELATIVE TO STREAM 1, INCLUDING THE SIGN
%
%If the lag is positive, it means you're predicting "ahead" of stream 1. So you
%take the data, chop the lag off the front, and slide stream 1
%forwards to line it up with the variables you're predicting.
%
%If the lag is negative, it means you're predicting "behind" of
%stream 1 (or you're predicting "ahead" of stream 2, the argument
%is reflexive). So you take the data, chop the lag off the front,
%and slide stream 2 forwards to line it up with the variables
%you're predicting. 
%
%Since this is all reflexive, we choose to take the frame numbers
%that are further along in time, ie. have higher time indexes. ie.
%we chop off the front.
%
%I'm explaining this so detailedly because I know it's going to
%confuse someone at a later date. And by someone I mean me.
function [lagged_data] = lag_and_pad(data,stream_1_idx, stream_2_idx, lag)

    if(lag>0)
        shifted_stream_1 = data(1:end-lag,stream_1_idx);
        truncated_data = data(lag+1:end,:);
        truncated_data(:,stream_1_idx) = shifted_stream_1;
    elseif(lag<0)
        lag = abs(lag); %otherwise you index as end+lag below. which would be silly.
        shifted_stream_2 = data(1:end-lag,stream_2_idx);
        truncated_data = data(lag+1:end,:);
        truncated_data(:,stream_2_idx) = shifted_stream_2;
    else
        truncated_data = data;%lag==0. do nothing.
    end

    lagged_data = truncated_data;
end