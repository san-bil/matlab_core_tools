%data_sequence_chunks in this case will be cell array of sequences. in the case of
%training_set, this will be many short sequences, for the case of
%the validation and test set, it will be one big chunk
function [lagged_sequences_collections] = lag_and_pad_at_different_intervals(sequences, stream_1_idx, stream_2_idx, lags)

    lagged_sequences_collections = cell(length(lags),1);

    for i = 1:length(lags)
        lag = lags(i);
        lagged_sequence_chunks = cell(size(sequences));

        for j = 1:length(sequences)
            sequence_chunk = sequences{j};
            lagged_chunk = lag_and_pad(sequence_chunk,stream_1_idx, stream_2_idx, lag);
            lagged_sequence_chunks{j} = lagged_chunk;
        end

        lagged_sequences_collections{i} = cell2mat(lagged_sequence_chunks');

    end


end