function out = condor_parfor_test_worker(in)

my_mult = in*2;
my_date = get_simple_date();
out = kv_create(my_mult,my_date);