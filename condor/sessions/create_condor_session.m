function session_object = create_condor_session(condor_task_root_dir, volatile_src_paths, options)

rsync_args = kv_get('condor_rsync_args',options,{});

volatile_src_task_path = [condor_task_root_dir filesep 'volatile'];
my_mkdir(volatile_src_task_path);


matlab_core_tools_dir = strsplit_inc_delim(mfilename('fullpath'),'matlab_core_tools');
volatile_src_paths{end+1} = matlab_core_tools_dir;
volatile_src_paths = my_unique(volatile_src_paths);
copy_files(volatile_src_paths,volatile_src_task_path,1,rsync_args);

%task_id = 1;
job_list={};
session_jobs_tags = {};
% session_object = kv_create(condor_task_root_dir, volatile_src_task_path, task_id, job_list, session_jobs_tags);

remote_submit = kv_get('remote_submit',options,0);
session_options = kv_create(remote_submit);

if(remote_submit)
    
    if(kv_haskey('remote_submit_host',options))
        remote_submit_host = kv_get('remote_submit_host',options);
    else
        remote_submit_host = get_good_doc_condor_submitter();
    end
    
    session_options = kv_set('remote_submit_host',remote_submit_host,session_options);
end


session_object = kv_create(session_options,condor_task_root_dir, volatile_src_task_path, job_list, session_jobs_tags);
