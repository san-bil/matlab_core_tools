remote_submit=1;
condor_obj = create_condor_session('/vol/bitbucket/sb1006/condor/1', {},kv_create(remote_submit));

condor_objs_acc = {};



for i = 1:25
   
    job_name = [num2str(i) '_'];
    tmp_condor_obj = submit_to_condor_session(condor_obj,  @condor_parfor_test_worker, {i}, {}, kv_create(job_name));
    condor_objs_acc{i} = tmp_condor_obj;
    
end

condor_obj = recombine_condor_session_objects(condor_objs_acc);

job_list = get_condor_session_job_list(condor_obj);

my_mults = collect_condor_session_results(job_list,'my_mult');
my_dates = collect_condor_session_results(job_list,'my_date');