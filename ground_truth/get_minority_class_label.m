function [minority_class_label, majority_class_label] = get_minority_class_label(ground_truth_column)

% in: a binary ground truth column
%
% out: the label of the class with lower incidence
%
% desc: (as above)
%
% tags: #ground_truth #labels

class_label_set = unique(ground_truth_column);
assert(length(class_label_set)==2)

minority_class_label = class_label_set((sum(ground_truth_column==class_label_set(1))>sum(ground_truth_column==class_label_set(2)))+1);%label of the class with fewer samples

majority_class_label = setdiff(class_label_set,minority_class_label);
