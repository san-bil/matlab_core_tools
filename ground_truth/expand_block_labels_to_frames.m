function ground_truth_frame_sequence = expand_block_labels_to_frames(block_labels, block_size)

% in: a list of labels, where each label corresponds to a fixed-length "block" of samples; the "block" size
%
% out: a list of labels, where each label corresponds to a sample
%
% desc: takes a time series of chunks or blocks, and a block size. expands each
% block into a subsequence of length block_size, where each value in the
% subsequence has value of its originating block
%
% e.g. expand_block_labels_to_frames([1, 2, 3], 3)
%                   --> [1 1 1 2 2 2 3 3 3]
%
% tags: #blocks #chunks #expand #labels #groundtruth #sequences #windows

ground_truth_frame_sequence = zeros(size(block_labels,1)*block_size,1);

for i = 1:length(block_labels)
    
    ground_truth_frame_sequence(((i-1)*block_size)+1:i*block_size,:) = ones(block_size,1)*block_labels(i);
    
end


end
