function dataframe=flip_class(dataframe,data_dictionary)

% in: a dataset and its associated data dictionary (where the dictionary is of form {'ground_truth_idx',ground_truth_col_idx; 'other_idx',other_col_idx...}
%
% out: the dataset with the ground truth "flipped", so the negative class is positive and vice-versa
%
% desc: flips the ground truth column of a dataset (assumes that ground truth column is in 0-1 format)
%
% tags: #groundtruth #labels #dataset

seq_column_idx = kv_get('ground_truth_idx',data_dictionary);

dataframe(:,seq_column_idx) = ~dataframe(:,seq_column_idx);


end
