function [ modified_dataset ] = convertTargetFormat( dataset, targetColumnIndex )
%CONVERTTARGETFORMAT Summary of this function goes here
%   Detailed explanation goes here

targets =  dataset(:,targetColumnIndex);

target_range = unique(targets);

if (target_range == [-1;1])
    targets(targets==-1) = targets(targets==-1).*0;
elseif (target_range == [0;1])
    targets(targets==0) = targets(targets==0)-1;
else
    bugger
end

modified_dataset = dataset;
modified_dataset(:,targetColumnIndex) = int32(targets);

end

