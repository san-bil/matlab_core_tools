function [ reformatted_dataset ] = change_label_format( dataset, label_column_idx, src_format, dst_format)

% in: dataset;
%     the column index of the ground truth column;
%     the current ground truth format; ('strictly_positive', 'non_negative', 'plus_minus')
%     the desired ground truth format; ('strictly_positive', 'non_negative', 'plus_minus')
%
% out: the dataset, with the format of ground truth column changed to the dst_format
%
% desc: Classification ground truth labels can be of the form {0,1}, {-1,+1}, {1,2} - this allows for changing between them.
%       as different toolboxes/packages require labels in different formats
%
% tags: #groundtruth #labels #dataset

targets =  dataset(:,label_column_idx);

if (strcmp(src_format,'strictly_positive'))
    
    if(strcmp(dst_format,'non_negative'))
        targets = targets - 1;
    elseif(strcmp(dst_format,'plus_minus'))
        targets = targets - 1;
        targets(targets==0) = targets(targets==0)-1;
    end
    
elseif(strcmp(src_format,'non_negative'))
    
    if(strcmp(dst_format,'strictly_positive'))
        targets = targets + 1;
    elseif(strcmp(dst_format,'plus_minus'))
        targets(targets==0) = targets(targets==0)-1;
    end
    
elseif(strcmp(src_format,'plus_minus'))
    
    if(strcmp(dst_format,'strictly_positive'))
        targets(targets==-1) = targets(targets==-1).*0;
        targets = targets + 1;
    elseif(strcmp(dst_format,'non_negative'))
        targets(targets==-1) = targets(targets==-1).*0;
    end
    
end

reformatted_dataset = dataset;
reformatted_dataset(:,label_column_idx) = int32(targets);

end

