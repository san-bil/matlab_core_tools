function ground_truth_frame_sequence = expand_sequence_labels_to_frames(sequence_labels, sequence_lengths)

% in: a list of labels, where each label corresponds to a variable-length "block" (sequence) of samples; the individual sequence lengths
%
% out: a list of labels, where each label corresponds to a sample
%
% desc: takes a list of sequence labels, and a vector of sequence lengths. Expands each
% sequence label into a label-sequence of length sequence_lengths(i), where each value in the
% subsequence has value of its originating block
%
% e.g. expand_sequence_labels_to_frames([1, 2, 3], [3 4 5])
%                   --> [1 1 1 2 2 2 2 3 3 3 3 3]
%
% tags: #sequences #expand #labels #groundtruth



ground_truth_frame_sequence = zeros(sum(sequence_lengths),1);

ground_truth_frame_sequence(1:sequence_lengths(1),:) = ones(sequence_lengths(1),1)*sequence_labels(1);

for i = 2:length(sequence_labels)
    
    marker_1 = sum(sequence_lengths(1:i-1))+1;
    marker_2 = sum(sequence_lengths(1:i));
    
    ground_truth_frame_sequence(marker_1:marker_2,:) = ones(sequence_lengths(i),1)*sequence_labels(i);
    
end


end
