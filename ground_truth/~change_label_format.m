function [ reformatted_labels ] = change_label_format( label_column, format )
%change_label_format() changes ground truth labels from {0,1} to {-1,+1}

if(format==[0 1])
    if(convert_to_row(unique(label_column))==[0 1])
        %Do nothing. the format is already correct
    else
        label_column(label_column==-1) = label_column(label_column==-1)+1;
        reformatted_labels=label_column;
    end
elseif(format==[-1 1])
    if(convert_to_row(unique(label_column))==[-1 1])
        %Do nothing. the format is already correct
    else
        label_column(label_column==0) = label_column(label_column==0)-1;
        reformatted_labels=label_column;
    end    
end



end

function i_vec = convert_to_row(i_vec)
    if(isrow(i_vec))
    else
        i_vec = i_vec';
    end
end