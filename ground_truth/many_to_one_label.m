function [ one_ts ] = many_to_one_label( many_ts )

% in: 2D matrix where each the sum of each row == 1 and each element is in {0,1}

% out: column vector where each entry is the index of the 
% nonzero entry in each row of the input 

% desc: turns a ground truth label matrix (where each class is represented by a column, and a 
% sample with that class has a 1 in that column) into a ground truth vector with numeric 
% ordinal class labels e.g.
%
%   [0 0 1]                 [3]
%   [0 1 0]     ---->       [2]
%   [0 0 1]                 [3]
%
% tags: #ground_truth #labels #class



[maxx,idx] = max(many_ts');
one_ts = idx';

one_ts = one_ts .* ~~(sum(many_ts,2));

end

