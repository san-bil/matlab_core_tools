function label_matrix = convert_labels_to_one_hot(ground_truth_col)

labels = unique(ground_truth_col);

if(min(labels)==0)
    ground_truth_col=ground_truth_col+1;
    labels = labels+1;
end
    
label_matrix = zeros(length(ground_truth_col),max(labels));
for i = 1:length(labels)
   label_matrix(ground_truth_col==labels(i),labels(i))=1;
end