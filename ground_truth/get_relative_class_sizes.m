function class_sizes = get_relative_class_sizes(ground_truth_col)

classes = unique(ground_truth_col);

if(sum(diff(classes)>1)>0)
    warning('all classes might not be present')
end

class_sizes = zeros(length(classes),1);

for i=1:length(classes)
    class_sizes(i) = sum(ground_truth_col==classes(i))/length(ground_truth_col);
end

end