function [ out_matrix ] = index_list_to_binary_matrix( in_vector )
%index_list_to_binary_matrix

out_matrix = zeros(max(in_vector));
for i = 1:size(in_vector,1)
    out_matrix(in_vector(i,1),in_vector(i,2))=1;
end
end

