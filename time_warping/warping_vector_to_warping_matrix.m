function warping_matrix = warping_vector_to_warping_matrix(warping_vector)

warping_matrix=index_list_to_binary_matrix([round(warping_vector) (1:length(warping_vector))']);
