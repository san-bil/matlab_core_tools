function columns_in_subplots(in, width)

if(~exist('width','var'))
   width=3; 
end

num_cols = size(in,2);

rows = ceil(num_cols/width);
figure;
for i = 1:num_cols

    subplot(rows, width, i);
    plot(in(:,i));
    ylim_set
    

end