function res= has_sound( input_video)

% in: the path to a video file
%
% out: a boolean, representing whether the video has an audio track
%
% desc: as above. Assumes that ffmpeg is installed, and on the user's path. 
% See http://geekswithblogs.net/renso/archive/2009/10/21/how-to-set-the-windows-path-in-windows-7.aspx if confused
%
% tags: #video #audio #sound

input_video = fix_path_slashes(input_video,1);

vdub_cmd = ['ffmpeg -i "' input_video '"'];

[call_res,output] = system(vdub_cmd);

res = ~(isempty([regexp(output,'audio') regexp(output,'Audio')]));
end

