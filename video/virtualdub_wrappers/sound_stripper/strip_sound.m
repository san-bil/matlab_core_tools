function vdub_cmd = strip_sound( input_video,output_video, dont_call)
%CUT_VIDEO Summary of this function goes here
%   Detailed explanation goes here
input_video = fix_path_slashes(input_video,1);
output_video = fix_path_slashes(output_video,1);

current_dir = get_parent_dir(mfilename('fullpath'));
vdub_script_template_path = [current_dir filesep 'sndstripper_template.vcf'];

[~,input_video_name] = get_parent_dir( input_video );
[~,output_video_name] = get_parent_dir( output_video );
[output_video_name_stem,~] = split_filename(output_video_name);

dictionary = {'INPUT_FILE_PATH',input_video;
              'OUTPUT_FILE_PATH',output_video;};

vdub_script_path = fix_path_slashes([current_dir filesep 'sndstrip_tmp' filesep output_video_name_stem '.vcf'],1);

Templater.fill(vdub_script_template_path,...
               dictionary,...
               vdub_script_path,...
               '\r\n');

           
vdub_cmd = ['C:\Users\Sanjay\Downloads\VirtualDub-1.10.4-AMD64\vdub64.exe /s ' unfix_path_slashes([vdub_script_path],1)];

if(exist('dont_call') && dont_call==1)
    %do nothing
else
    [call_res,output] = system(vdub_cmd);
end

end

