function im_seq_to_video( input_folder,output_video,frame_rate)

% in: - the path to a video file
%     - start frame index
%     - end frame index
%     - output video clip path
%     - codec string (optional) 
% out: nada
%
% desc: creates a temporary virtualdub script in the (relative to this file) ./vidcut_tmp folder, and calls virtual dub
%       to cut the video at the specified frame indices. obviously assumes that you have virtualdub "installed" (i.e. unzipped) and the executable path appended
%       to your PATH environment-variable (since I don't specify an executable path)
%
% tags: #video #audio #sound #editing

if(ispc)
    input_folder = fix_path_slashes(input_folder,1);
    output_video = fix_path_slashes(output_video,0);

    if(~exist(output_video,'dir'))
        mkdir(output_video);

        current_dir = get_parent_dir(mfilename('fullpath'));
        vdub_script_template_path = [current_dir '/' 'im_seq_to_video_template.vcf'];

        [~,input_video_name] = get_parent_dir( input_folder );
        [output_video_name_stem,~] = split_filename(input_video_name);

        dictionary = {
                      'INPUT_DIR',input_folder;
                      'OUTPUT_VIDEO',output_video;
                      'FRAME_RATE',output_video
                      };

        vdub_script_path = [current_dir '/' 'im_seq_to_video_tmp' '/' regexprep(output_video_name_stem,' ','_') '.vcf'];

        Templater.fill(vdub_script_template_path,...
                       dictionary,...
                       vdub_script_path,...
                       '\r\n');
        
        
        [jnk,username] = system('echo %USERNAME%');
    
        if(strcmp(strtrim(username),'Sanjay'))
            vdub_cmd = ['C:\Users\Sanjay\Dropbox\PhD\PhD_Year_1\ibug_tools\VirtualDub-1.9.11\vdub.exe /s '];
        else
            vdub_cmd = 'vdub.exe /s ';
        end

        vdub_cmd = [vdub_cmd  unfix_path_slashes(vdub_script_path,1)];
        [call_res,output] = system(vdub_cmd);
    end
else
    error('This converter is a wrapper for VirtualDub, which is only available for Windows. Sorry :(')
end

end

