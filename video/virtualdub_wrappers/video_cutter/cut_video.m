function cut_video( input_video,start_frame,end_frame,output_video,codec)

% in: - the path to a video file
%     - start frame index
%     - end frame index
%     - output video clip path
%     - codec string (optional) 
% out: nada
%
% desc: creates a temporary virtualdub script in the (relative to this file) ./vidcut_tmp folder, and calls virtual dub
%       to cut the video at the specified frame indices. obviously assumes that you have virtualdub "installed" (i.e. unzipped) and the executable path appended
%       to your PATH environment-variable (since I don't specify an executable path)
%
% tags: #video #audio #sound #editing #cutting


input_video = fix_path_slashes(input_video,1);
output_video = fix_path_slashes(output_video,1);


current_dir = get_parent_dir(mfilename('fullpath'));
vdub_script_template_path = [current_dir filesep 'cutter_template.vcf'];

[~,input_video_name] = get_parent_dir( input_video );
[~,output_video_name] = get_parent_dir( output_video );
[output_video_name_stem,~] = split_filename(output_video_name);

dictionary = {'INPUT_FILE_PATH',input_video;
              'START_FRAME',num2str(start_frame);
              'END_FRAME',num2str(end_frame);
              'OUTPUT_FILE_PATH',output_video;};

vdub_script_path = [current_dir filesep 'vidcut_tmp' filesep output_video_name_stem '.vcf'];

Templater.fill(vdub_script_template_path,...
               dictionary,...
               vdub_script_path,...
               '\r\n');

           
vdub_cmd = ['vdub.exe /s ' unfix_path_slashes(vdub_script_path,1)];

[call_res,output] = system(vdub_cmd);

end

%cut_video('D:\data\detectors_training\semaine\2009.01.30.12.00.35_User_Frontal_C_Prudence.avi',1000,1500,'D:\data\detectors_training\nod_training1\test.avi')
%
%
%
%
%