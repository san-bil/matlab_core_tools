function [ all_ts ] = frame_to_time_secs( frames,frame_rate )

% in: a vector containing frame indices
%     a frame rate (i.e. sampling frequency)
%
% out: a cell array containing string timestamps
%
% desc: converts frame indices to timestamps with format XmYsZms, given the framerate
%
% tags: #times #videos #timestamps

if nargs<2
    frame_rate = 58
end

all_ts = {};

for i = 1:length(frames)
    
    all_ts{i} = f2ts(frames(i));
    
end

all_ts = all_ts';

    function ts = f2ts(frame)
        
        ts = [num2str(floor(floor(frame/frame_rate)/60)) 'm ' num2str(mod(floor(frame/frame_rate),60)) 's ' num2str(((frame/frame_rate) - floor(frame/frame_rate))*1000,3) 'ms'];
        
    end

end

