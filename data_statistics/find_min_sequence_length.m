function [min_sequence_length,sequence_lengths] = find_min_sequence_length(data,data_dictionary)
%
%
% in: a dataset that must contain a column with sequence numbers; 
%     a data dictionary with key 'sequence_idx', mapping to the index of the column containing the sequence numbers 
%
% out: the smallest sequence length in the dataset;
%      a list of all sequence lengths in the dataset
%       
% desc: finds the shortest sequence length in a dataset, given it has a sequence column included
% e.g. find_min_sequence_length(
%                               [0.8147    0.1576    0.6557   1
%                                0.9058    0.9706    0.0357   1
%                                0.1270    0.9572    0.8491   1
%                                0.9134    0.4854    0.9340   2
%                                0.6324    0.8003    0.6787   2
%                                0.0975    0.1419    0.7577   2
%                                0.2785    0.4218    0.7431   2
%                                0.5469    0.9157    0.3922   2
%                                0.9575    0.7922    0.6555   3
%                                0.9649    0.9595    0.1712   3],
%                               {'sequence_idx',4})
%
%   would return 2)
%
% tags: #sequences #sequencelengths #metadata
%
%

sequence_column = data(:,kv_get('sequence_idx',data_dictionary));

sequence_numbers = my_unique(sequence_column);

sequence_lengths = zeros(size(sequence_numbers));

for i = 1:length(sequence_numbers)

    sequence = sequence_column(sequence_column==sequence_numbers(i));
    sequence_lengths(i) = length(sequence);

end

min_sequence_length = min(sequence_lengths);
