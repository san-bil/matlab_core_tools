function [ seq_class_ratio,frame_class_ratio ] = get_ground_truth_statistics( seq_col,ground_truth_col )

% in: a sequence column; a binary class vector
%     e.g. [1 1 1 1 1 2 2 2 3 3 3 3 3 3 3 3 3 4 4 4 4 5 6 6 6 6 6 6 6 6 6] and
%          [1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 0 0 0 0]
%
% out: neg:pos class ratio (sequence level); neg:pos class ratio (frame level)
%
% desc: calculates the negative:positive class ratio at the sequence and frame levels
%
% tags: #groundtruth #metadata #classproportions #classes #counting #sequences

pos_data_seq_col = seq_col(ground_truth_col==1);
neg_dat_seq_col =  seq_col(ground_truth_col==0);

num_pos_sequences = length(get_sequence_lengths(pos_data_seq_col));
num_neg_sequences = length(get_sequence_lengths(neg_dat_seq_col));

seq_class_ratio = [num_neg_sequences,num_pos_sequences];

pos_data_ground_truth_col = ground_truth_col(ground_truth_col==1);
neg_data_ground_truth_col =  ground_truth_col(ground_truth_col==0);

frame_class_ratio = [size(neg_data_ground_truth_col,1),size(pos_data_ground_truth_col,1)];


end

