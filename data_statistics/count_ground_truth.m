
sum(dataframe(:,kv_get('ground_truth_idx',idxs))) %num positive labels (where positive is assumed to be "1")

abs(sum(dataframe(:,kv_get('ground_truth_idx',idxs))-1)) %num negative labels (where negative is assumed to be "0")

assert(abs(sum(dataframe(:,kv_get('ground_truth_idx',idxs))-1))== size(dataframe,1)-sum(dataframe(:,kv_get('ground_truth_idx',idxs))))