function class_proportions=get_class_proportions(ground_truth_column)
    
% in: a binary column vector (representing classes)
%
% out: the negative:positive class proportions
%
% desc: (as above)
%
% tags: #groundtruth #metadata #classproportions #classes #counting

class_proportions = [(1-sum(ground_truth_column)/length(ground_truth_column)), sum(ground_truth_column)/length(ground_truth_column)];

end
