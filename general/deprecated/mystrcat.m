function [ out_string ] = mystrcat(varargin)
% concatenates strings, deprecated since you can cat strings using [str1 str2]

string_accumulator = {''};

for i = 1:nargin
   string = varargin{i}; 
   boxed_string = {string};
   string_accumulator = strcat(string_accumulator,boxed_string);   
end

out_string = string_accumulator{1};

end

