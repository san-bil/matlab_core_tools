function opposite_op = get_opposite_binop(binop)

if(strcmp(func2str(binop),'gt'))
    
    opposite_op = @lt;
    
elseif(strcmp(func2str(binop),'lt'))

    opposite_op = @gt;
else
    error('Initial binop must be lt or gt.')
end