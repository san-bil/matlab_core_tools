function [ cube ] = matrix_to_3_tensor( flat, slice_y_size )

% in: 2D matrix, where each contiguous block of slice_y_size rows represents one group
% of observations, corresponding to e.g. a time index

% out: a 3-tensor, where each slice (in the z-direction) is a 
% contiguous block of slice_y_size rows

% desc: Takes a 2D array, where each observation is a group of rows, chops up array into  
% those groups, and stacks those groups front to back.

% tags: #tensor #reshape #dataset #spatiotemporal

x_dim = size(flat,2);
y_dim = slice_y_size;
z_dim = size(flat,1)/slice_y_size;

cube = zeros(y_dim,x_dim,z_dim);

for i = 1:z_dim
    frame_block_start = ((i-1)*slice_y_size)+1;
    frame_block_end = i*slice_y_size;
    
    frame_block = flat(frame_block_start:frame_block_end,:);
    cube(:,:,i) = frame_block;
    
end


end

