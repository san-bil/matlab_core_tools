function [ flattened_cube ] = flatten_3_tensor( cube )

% in: 3-tensor

% out: 2D matrix, where each contiguous block of rows is a z-direction slice of the former 3-tensor

% desc: Flattens a 3-tensor to a 2D matrix, where the z-directional "slices" of the 3D
% matrix are concatenated vertically downwards
%
% tags: #tensor #flatten #spatiotemporal





x_dim = size(cube,2);
y_dim = size(cube,1)*size(cube,3);


flattened_cube = zeros(y_dim,x_dim);

for i = 1:size(cube,1)
    frame_block_start = ((i-1)*size(cube,1))+1;
    frame_block_end = i*size(cube,1);
    
    frame_block = cube(i,:,:);

    rs_frame_block = permute(frame_block,[3 2 1]);
    
    flattened_cube(frame_block_start:frame_block_end,:) = rs_frame_block;
    
end


end

