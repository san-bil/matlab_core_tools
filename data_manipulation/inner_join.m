function [ joined_data ] = inner_join( dataframe_1, key_col_idx1, dataframe_2, key_col_idx2 )

% in: (dataset and join column) x2

% out: joined dataset

% desc: Matlab equivalent of a database join on a column; 
% Given two arrays and the column index (in each) that you want to 
% join on (since there are no column headers in Matlab), joins the two "tables".
%
% tags: #join #tables

[idxa, idxb] = ismember(dataframe_1(:,key_col_idx1), dataframe_2(:,key_col_idx2));

idxb(idxb==0) = [];
joined_data = [dataframe_1(idxa,:) dataframe_2(idxb,:)];

end

