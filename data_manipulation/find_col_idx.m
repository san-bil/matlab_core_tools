function idx = find_col_idx(col_header,data_array)

% in: column header/variable name to find; cell array where the first row contains the column headers
%
% out: corresponding column index
%
% desc: Given a cell array where the first row contains the column headers, and a string to match, returns the % column index where the header equals that string. Returns 0 otherwise.
%
% tags: #variablename #columnheader #column_header #columns


idx = 0;
for i = 1:size(data_array,2)

    ch = data_array{1,i};
    
    if(strcmp(ch,col_header))
        idx = i;
    end

end

end
