function out = split_data_by_proportion(x, weights, split_ids)

    cnt=1;
    if(isvector(x))
        x_size=length(x);
    else
        x_size=size(x,1);
    end
    
    normalized_proportions = ceil((weights / sum(weights))*x_size);
    split_points=[1 cumsum(normalized_proportions)+1];
    xs={};
    for i = 1:(length(split_points)-1)
        if(isvector(x))
            xs{i} = x(min(x_size,split_points(i)):min(x_size,(split_points(i+1)-1)));
        else
            xs{i} = x(min(x_size,split_points(i)):min(x_size,(split_points(i+1)-1)),:);
        end
        
    
    end
    
    if exist('split_ids','var')
        out = [split_ids' xs'];
    else
        out = xs';
    end
end