function [ Y ] = mean_centre( X, direction )

% in: observation matrix, rows are observations, columns are variables
%
% out: observation matrix, where columns are mean-shifted (but not scaled)
%
% desc: as above
%
% tags: #normalization #zscore #scaling
mean_vec = mean(X,direction);

if(direction==1)
    mean_array_dim = [size(X,1),1];
else
    mean_array_dim = [1,size(X,2)];    
end

Y = X - repmat(mean_vec,mean_array_dim);

end

