function normalized_array = median_zscore(array)

% in: observation matrix, rows are observations, columns are variables
%
% out: observation matrix, where columns are median-shifted and scaled to have unit variance
%
% desc: as above
%
% tags: #normalization #zscore #scaling #median

median_array = repmat(median(array),size(array,1),1);

normalized_array = array-median_array;

st_dev_array = repmat(std(normalized_array),size(array,1),1);
normalized_array = normalized_array./st_dev_array;
