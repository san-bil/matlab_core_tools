function normalized_array = median_iqr_normalize(array)

% in: observation matrix, rows are observations, columns are variables
%
% out: observation matrix, where columns have MEDIAN subtracted and are scaled to have unit INTER-QUARTILE RANGE 
%
% desc: as above
%
% tags: #normalization #zscore #scaling #median #iqr #interquartilerange

median_array = repmat(median(array),size(array,1),1);

normalized_array = array-median_array;

st_dev_array = repmat(iqr(normalized_array),size(array,1),1);
normalized_array = normalized_array./st_dev_array;
