function resampled_dataset = balance_dataset(dataset,data_dictionary,other_options)

seq_or_frame_balance = kv_get('seq_or_frame_balance',other_options);

real_dataset_length = size(dataset,1);

ground_truth_idx = kv_get('ground_truth_idx',data_dictionary);
sequence_column_idx = kv_get('sequence_idx',data_dictionary);
frame_column_idx = kv_get('frame_idx',data_dictionary);

ground_truth_column = dataset(:,ground_truth_idx);
sequence_column = dataset(:,sequence_column_idx);
frame_column = dataset(:,frame_column_idx);

class_label_set = unique(ground_truth_column);
minority_class_label = class_label_set((sum(ground_truth_column==class_label_set(1))>sum(ground_truth_column==class_label_set(2)))+1);
majority_class_label = setdiff(class_label_set,minority_class_label);

majority_class_sequence_numbers = my_unique(sequence_column(ground_truth_column==0));
minority_class_sequence_numbers = my_unique(sequence_column(ground_truth_column==1));

majority_class_sequence_lengths = get_sequence_lengths(sequence_column(ground_truth_column==majority_class_label));
minority_class_sequence_lengths = get_sequence_lengths(sequence_column(ground_truth_column==minority_class_label));

num_majority_class_frames = sum(majority_class_sequence_lengths);
num_minority_class_frames = sum(minority_class_sequence_lengths);
num_majority_class_sequences = length(majority_class_sequence_lengths);
num_minority_class_sequences = length(minority_class_sequence_lengths);


frame_imbalance = num_majority_class_frames - num_minority_class_frames;
sequence_imbalance = num_majority_class_sequences - num_minority_class_sequences;

if(seq_or_frame_balance=='seq')
    resampling_sequence_number_idxs = ceil(rand(sequence_imbalance,1)*length(minority_class_sequence_numbers));
    resampled_sequence_numbers = minority_class_sequence_numbers(resampling_sequence_number_idxs);
    resampled_sequence_lengths = minority_class_sequence_lengths(resampling_sequence_number_idxs);
   

elseif(seq_or_frame_balance=='frame')
    resampled_sequence_numbers = [];
    resampled_sequence_lengths = [];
    while(1)
        if(sum(resampled_sequence_lengths)>=frame_imbalance)
            break;
        else
            resampled_sequence_number = round(rand()*length(minority_class_sequence_numbers));
            resampled_sequence_length = minority_class_sequence_lengths(minority_class_sequence_numbers==resampled_sequence_number);
            resampled_sequence_numbers = [resampled_sequence_numbers; resampled_sequence_number];
            resampled_sequence_lengths = [resampled_sequence_lengths; resampled_sequence_length];
        end        
    end
else
    error(['balance_dataset() - seq_or_frame_balance ''' seq_or_frame_balance ''' is not valid'])
end

last_real_sequence_num = sequence_column(end);
last_real_frame_num = frame_column(end);

resampled_block = zeros(sum(resampled_sequence_lengths),size(dataset,2));
bookmark = 1;
for k = 1:length(resampled_sequence_numbers)
    
    sub_block = dataset(sequence_column==resampled_sequence_numbers(k),:);
    
    sub_block_sequence_col = ones(size(sub_block,1),1)*(last_real_sequence_num+k);
    sub_block_frame_col = (1:resampled_sequence_lengths(k))+last_real_frame_num+bookmark-1; 
    
    sub_block(:,kv_get('sequence_idx',data_dictionary)) = sub_block_sequence_col;
    sub_block(:,kv_get('frame_idx',data_dictionary)) = sub_block_frame_col;

    
    resampled_block(bookmark:resampled_sequence_lengths(k)+bookmark-1,:) = sub_block;
    bookmark = bookmark+resampled_sequence_lengths(k);
end

 resampled_dataset = [dataset; resampled_block];