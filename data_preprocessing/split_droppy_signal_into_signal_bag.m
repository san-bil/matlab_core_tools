function signal_bag = split_droppy_signal_into_signal_bag(droppy_signal)

% in: a 1d signal
%
% out:  a cell array containing discontinuous sequences of the signal, between which signal was lost (i.e. was nan)
%
% desc: as above
%
% tags: #nan #defensive #cleaning #nanremoval

sequence_lengths = get_sequence_lengths(generate_dummy_sequence_numbers([1; isnan(droppy_signal)]));
clean_idxs = isnan(droppy_signal); 
nan_idxs = ~isnan(droppy_signal);

signal_bag = {};
for i = 2:length(sequence_lengths)
    if(mod(i,2)==0)
        signal_bag{end+1} = droppy_signal(sum(sequence_lengths(1:i-1)):(sum(sequence_lengths(1:i)))-1);
    end
end
