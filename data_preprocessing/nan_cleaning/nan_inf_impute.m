function imputed_matrix = nan_inf_impute(matrix, impute_method)

% in: numeric matrix, where each column represents a variable
%
% out: numeric matrix, where nan values for a given variable are replaced by some imputed value of variable (default: mean-average)
%
% desc: (as above)
%
% tags: #nan #cleaning #inf #nanremoval

if(~exist('impute_method','var'))
    impute_method = str2func('mean');
end

imputed_matrix = zeros(size(matrix));

for i = 1:size(matrix,2)

    column = matrix(:,i);
    
    [filtered_col,nan_inf_idxs] = (nan_inf_filter(column));
    
    filtered_col_mean = impute_method(filtered_col);
    
    imputed_column = column;
    imputed_column(nan_inf_idxs) = filtered_col_mean;
    
    imputed_matrix(:,i) = imputed_column;

end


end
