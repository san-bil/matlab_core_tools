function [filtered_arr,nan_inf_idxs] = nan_inf_filter_array(arr)

% in: numeric arr
%
% out: numeric arr with nan values removed
%
% desc: removes nan and infinity values from arr. may flatten nD matrices as a side-effect.
%
% tags: #nan #cleaning #inf #nanremoval

indicator_column = sum(arr,2);
nan_inf_idxs = find(isnan(indicator_column) | isinf(indicator_column));
clean_idxs = setdiff(1:size(arr,1), nan_inf_idxs);
filtered_arr = arr(clean_idxs,:);


end
