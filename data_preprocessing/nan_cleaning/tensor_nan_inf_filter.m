function [ applied_tensor,min_idx,end_distance ] = tensor_nan_inf_filter( tensor )

% in: a 3-tensor with a lot of nan values at the end
%
% out: a 3-tensor with those nan values removed, by truncating the z-slices
%
% desc: assuming the z-slices with nan values are at the end of the tensor, removes those slices
%
% tags: #nanremoval #nan #cleaning #spatiotemporal

min_idxs = [];
for i=1:size(tensor,1)
    for j=1:size(tensor,2)
        vec=tensor(i,j,:);
        rotated_vec = permute(vec,[3,1,2]);
        [~,nan_idxs] = nan_inf_filter(rotated_vec);
        min_idxs = [min_idxs min(nan_idxs)];
    end
end

min_idx = min(min_idxs);
if(~isempty(min_idx))
    applied_tensor=tensor(:,:,1:min_idx-1);
    end_distance = (size(tensor,3)-min_idx)+1;
else
    applied_tensor=tensor;
end

end

