function [filtered_arr,nan_inf_idxs] = nan_inf_filter(arr)

% in: numeric arr
%
% out: numeric arr with nan values removed
%
% desc: removes nan and infinity values from arr. may flatten nD matrices as a side-effect.
%
% tags: #nan #cleaning #inf #nanremoval

filtered_arr = (arr(~isnan(arr) & ~isinf(arr)));

nan_inf_idxs = find(~((~isnan(arr) & ~isinf(arr))));

end
