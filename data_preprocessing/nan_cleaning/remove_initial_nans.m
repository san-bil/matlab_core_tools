function [ cleaned_dataframe,num_rows_removed,dirty_row_idxs ] = remove_initial_nans( dataframe,col_idxs)

% in: numeric matrix, where each column represents a variable; columns to consider
%
% out: numeric matrix, where nan values for a given variable are replaced by some imputed value of variable (default: mean-average)
%
% desc: Given a dataframe with NaNs in it [which can screw up an
% entire column when said column is zscored (mean shifted, std normalized)]
% or otherwise processed using global statistics, removes the initial rows which have NaNs in them 
% limited to the columns specified in col_idxs (default: all columns)
%
% tags: #nan #cleaning #inf #nanremoval


if ~exist('col_idxs', 'var') || isempty(col_idxs)
    col_idxs = 1:size(dataframe,2);
end

clean_row_idxs = [];
dirty_row_idxs = [];
for i = 1:size(dataframe,1)
    row = dataframe(i,col_idxs);
    if(sum(isnan(row))==0)
        clean_row_idxs = [clean_row_idxs; i];
    else
        dirty_row_idxs = [dirty_row_idxs; i];
    end
end

cleaned_dataframe = dataframe(clean_row_idxs,:);
num_rows_removed = length(dirty_row_idxs);

end

