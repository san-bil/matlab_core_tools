function [ cleaned_dataframe ] = impute_nans( dataframe )

% in: 2D matrix
%
% out: nan-free-2D-matrix
%
% desc: Given a dataframe with NaNs in it [which can screw up an
% entire column when said column is zscored (mean shifted, std normalized)],
% replaces those NaNs with nearest non-NaN value. if the entire row is nan, then 
% the entire row is replaced with the nearest non-nan-containing row. If only some variables are nan, 
% then the sample with the closest cosine similiarity is found, and used to fill in the nan variables.
%
% tags: #nan #cleaning #inf #nanremoval

cleaned_dataframe = dataframe;

for i = 1:size(dataframe,1)

    row = dataframe(i,:);

    if(sum(isnan(row))==length(row))

        for j = 1:size(dataframe,1)
            if(i~=j)
                candidate_row = dataframe(j,:);
                if(sum(isnan(candidate_row))==0)
                    cleaned_dataframe(i,:) = candidate_row;
                    break;
                end
            end
        end

    elseif(sum(isnan(row))>0)

        for j = 1:size(dataframe,1)
            max_similiarity = -bitmax;
            most_similiar_row_idx = -bitmax;
            if(i~=j)
                candidate_row = dataframe(i,:);
                candidate_similiarity = nan_robust_cosine_similiarity(row,candidate_row);
                if(candidate_similiarity>max_similiarity)
                    max_similiarity = candidate_similiarity;
                    most_similiar_row_idx = j;
                end
            end
        end

        nan_entry_idxs = isnan(row);
        cleaned_dataframe(i,nan_entry_idxs) = dataframe(most_similiar_row_idx,nan_entry_idxs);

    end


end


end

