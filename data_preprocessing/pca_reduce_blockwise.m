function [ new_data ] = pca_reduce_blockwise(new_dim, chunking_column_name, columns_name, data_dictionary, dataframe)

assert(new_dim>1,'new dimensionality must be greater than 1 - specifying a cumulative energy might cause different chunks to have different dimensionalities.')

uniques = get_dataframe_column_uniques(chunking_column_name,data_dictionary,dataframe);
new_data = [];


energies = [];
for u = 1:length(uniques)
   unique_value = uniques(u);
   [chunk,chunk_row_idxs] = filter_dataframe(chunking_column_name,data_dictionary,@(x)x==unique_value,dataframe);
   chunk_columns = get_dataframe_column(columns_name,data_dictionary,chunk);
   [ reduced_chunk, total_energy ] = pca_reduce(chunk_columns,new_dim);
   new_data = [new_data ; reduced_chunk];
   energies(end+1) = total_energy;
end

