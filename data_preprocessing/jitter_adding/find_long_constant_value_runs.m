function [long_sequence_lengths,long_sequence_idxs] = find_long_constant_value_runs(full_sequence, run_length_threshold)

full_sequence_length = length(full_sequence);
cv_run_sequence_lengths = [];
cv_run_sequence_idxs = {};
i=2;

prev = full_sequence(1);

curr_sequence_length = 1;
curr_sequence_idxs = [1];

while(1)
    curr = full_sequence(i);
    
    if(abs(curr-prev)<0.000000000001)
        curr_sequence_idxs = [curr_sequence_idxs i];
    else
        cv_run_sequence_lengths = [cv_run_sequence_lengths length(curr_sequence_idxs)];
        cv_run_sequence_idxs = [cv_run_sequence_idxs;{curr_sequence_idxs}];
        
        curr_sequence_idxs=[i];
    end

    prev = curr;
    
    i=i+1;
    
    if(i==full_sequence_length+1)
        cv_run_sequence_lengths = [cv_run_sequence_lengths length(curr_sequence_idxs)];
        cv_run_sequence_idxs = [cv_run_sequence_idxs;{curr_sequence_idxs}];
        break;
    end
    
    if(i>full_sequence_length+1)
        error('find_long_constant_value_runs() error')
    end
    
end


long_sequence_lengths_idxs = cv_run_sequence_lengths>run_length_threshold;

long_sequence_lengths = cv_run_sequence_lengths(long_sequence_lengths_idxs);
long_sequence_idxs = cv_run_sequence_idxs(long_sequence_lengths_idxs);