function noise_added_dataframe = add_noise_to_long_constant_value_runs(threshold_length, dataframe)

% in: an observation matrix where rows are observations, columns are variables; a threshold defining what a "long constant value run is" 
%
% out: the observation matrix where (per column) long sequences of constant value have jitter added to them.
%
% desc: given an observation matrix (rows are observations, columns are variables), 
% some columns may have long sequences of constant value - this may be due to nan removal
% where the average of the column is imputed. when taking window-wise measures, this can cause a 
% variance == 0, causing a divide-by-zero when z-scoring data. this adds random jitter to the "constant-value-run"
% where the jitter is sampled from a zero-mean gaussian with variance taken from a surrounding window

%
% tags: #jitter #noiseaddition #dividebyzero

noise_added_dataframe = zeros(size(dataframe));


    
parfor j = 1:size(dataframe,2)

    stream_col = dataframe(:,j);
    [cv_run_lengths,cv_run_idxs] = find_long_constant_value_runs(stream_col,threshold_length);
    
    for k = 1:length(cv_run_lengths)
        run_idx = cv_run_idxs{k};
        
        try
            surrounding_window_idx = find_surrounding_window_boundaries(min(run_idx),max(run_idx),threshold_length,length(stream_col),0);
            surrounding_window_variance = std(stream_col(surrounding_window_idx));
        catch err
            print(err)
            surrounding_window_variance = 0.001;    
        end
        
        stream_col(cv_run_idxs{k}) = stream_col(cv_run_idxs{k})+normrnd(0,surrounding_window_variance/20,length(cv_run_idxs{k}),1);
    end
    
    noise_added_dataframe(:,j) = stream_col;

end



