function [ new_data, total_energy ] = pca_reduce(data,new_dim)




[coeff, score, latent, tsquared, explained, mu] = pca(data);

if(new_dim<1)
    energy_quantile = new_dim;
    total_energy=energy_quantile;
    normalized_cumulative_eigenvalues = cumsum(latent)/sum(latent);
    energy_quantile_idx = head(find(normalized_cumulative_eigenvalues>energy_quantile),1);
    new_data = score(:,1:energy_quantile_idx);
else
    new_data = score(:,1:new_dim);
    total_energy = cumsum(latent(1:new_dim));
end


end

