function downsample_image_sequence(image_sequence_folder,n)

% in: a fully-qualified folder path containing an image sequence
%
% out: nada
%
% desc: downsamples the image sequence contained in the folder i.e. removes n-1 out of n frames
% so calling downsample_image_sequence('~/tmp',3) where ~/tmp contains [1.jpg, 2.jpg, 3.jpg, 4.jpg, 5.jpg, 6.jpg, 7.jpg, 8.jpg]
% would remove everything but [1.jpg, 4.jpg, 7.jpg]
%
%
% tags: #sampling #subsampling #downsampling #imagesequence #video #framerate


if(~exist('n','var'))
    n = 2;
end

files = ls(image_sequence_folder);

sorted_files = sort_nat(cellfun(@strtrim,(mat2cell(files,ones(size(files,1),1),size(files,2))),'UniformOutput',0));


for i = 3:length(sorted_files)
   
    name = sorted_files(i,:);
    if(mod((i-2),n)==0)
            delete([image_sequence_folder '\' name{1}]);
    
    end
    
end

end
