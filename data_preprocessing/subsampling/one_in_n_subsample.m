function [ sampled_data ] = one_in_n_subsample( dataset, n )
%
% in: observation matrix of size N
%
% out: observation matrix of size N/n
%
% desc: samples data deterministically in a "one in n" manner, i.e. for all i such that mod(i,5)==0, so 
% one_in_n_subsample( [1 1 1 1 1 2 2 2 2 2 3 3 3 3 3 4 4 4 4 4]', 5) would give [1 2 3 4 5]
%
% tags: #sampling #subsampling #downsampling

index_vec = 1:n:size(dataset,1);

sampled_data = dataset(index_vec,:);

end

