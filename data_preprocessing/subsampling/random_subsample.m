function [ subsampled_dataset ] = random_subsample( dataset,num_samples )

% in: observation matrix, rows are samples, columns are variables; num_samples is size of subset required
%
% out: subset of observation matrix, where sampling is done by rows 
%
% desc: takes a random subset (without replacement) of the rows of the dataset (each row is a sample)
%
% tags: #randomsampling #sampling #subsampling #downsampling


index_vec = randsample(size(dataset,1),num_samples);
subsampled_dataset = dataset(index_vec,:);


end

