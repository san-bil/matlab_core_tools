function [sorted_files, idxs] = sort_file_list_numerically(files)

% in: a list of filenames
%
% out: the list of filenames, sorted numerically (assuming they have a numerically-meaningful
% filename
%
% desc: as above.
%
% tags: #sorting #files #filenames

[sorted,idxs] = sort(files);

sorted_files = files(idxs);