function [out,matching_rows_idxs]=filter_dataframe(col_key,data_dictionary,filter_fhandle,dataframe)

col_idx = kv_get(col_key,data_dictionary);
col = dataframe(:,col_idx);
matching_rows_idxs = filter_fhandle(col);
out = dataframe(matching_rows_idxs,:);

end
