function [dataframe,other_outputs_acc] = apply_to_columns_blockwise(chunking_column_name, columns_name,data_dictionary,func_handle, dataframe)


uniques = get_dataframe_column_uniques(chunking_column_name,data_dictionary,dataframe);
other_outputs_acc = {};
for u = 1:length(uniques)
    
   unique_value = uniques(u);
   [chunk,chunk_row_idxs] = filter_dataframe(chunking_column_name,data_dictionary,@(x)x==unique_value,dataframe);
   [modified_chunk,other_outputs] = apply_to_columns(columns_name,data_dictionary,func_handle,dataframe);
   dataframe(chunk_row_idxs,:) = modified_chunk;
   
end