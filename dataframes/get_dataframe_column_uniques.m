function out=get_dataframe_column_uniques(col_key,data_dictionary,dataframe)

col = get_dataframe_column(col_key,data_dictionary,dataframe);
out = my_unique(col);

end
