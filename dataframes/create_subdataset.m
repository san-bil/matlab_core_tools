function [subdataset,new_data_dictionary] = create_subdataset(required_col_names,data_dictionary,dataset)

subdataset=[];
new_data_dictionary={};
for i = 1:length(required_col_names)    
    col_data = get_dataframe_column(required_col_names{i}, data_dictionary, dataset);
    [subdataset,new_col_idxs] = add_to_dataframe( subdataset, col_data );
    new_data_dictionary = kv_set(required_col_names{i},new_col_idxs,new_data_dictionary);
end

end