function cols=get_dataframe_columns(col_key,data_dictionary,dataframe, delim)

if(~exist('delim','var'))
    delim = ';';
end

col_keys = strsplit(col_key,delim);

cols = [];

for i = 1:length(col_keys)
    tmp_key = col_keys{i};
    col_idx = kv_get(tmp_key,data_dictionary);
    col = dataframe(:,col_idx);
    cols = [cols col];
end

end
