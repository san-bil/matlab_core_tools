function [dataframe,data_dictionary] = add_to_dataframe_multi( dataframe, data_dictionary, varargin )

% in: dataset (rows=samples, columns=variables);
%     new columns to add to dataset
%
% out: new dataset, with columns added;
%      indices in new dataset of the added columns, for book-keeping (i.e. constructing a data dictionary)
%
% desc: adds the columns to the dataset by horizontal concatenation, 
% but also tracks the column numbers in the new dataset of the added columns, should you
% want to construct a data dictionary 
%
% tags: #dataset #horzcat #bookkeeping #metadata

for i = 1:2:length(varargin)
    
    idx_name = varargin{i};
    columns_to_add = varargin{i+1};
    
    [dataframe, columns_idx] = add_to_dataframe(dataframe, columns_to_add);
    data_dictionary = kv_set(idx_name,columns_idx,data_dictionary);
    
end
