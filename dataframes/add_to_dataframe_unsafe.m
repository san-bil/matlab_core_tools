function [updated_dataframe,column_numbers] = add_to_dataframe_unsafe( dataframe, columns_to_add, discrepancy_tolerance_ratio, manual_override, take_larger )

% in: dataset (rows=samples, columns=variables);
%     new columns to add to dataset
%
% out: new dataset, with columns added;
%      indices in new dataset of the added columns, for book-keeping (i.e. constructing a data dictionary)
%
% desc: adds the columns to the dataset by horizontal concatenation, 
% but also tracks the column numbers in the new dataset of the added columns, should you
% want to construct a data dictionary 
%
% tags: #dataset #horzcat #bookkeeping #metadata

if(~exist('discrepancy_tolerance_ratio','var'))
    discrepancy_tolerance_ratio = 0.0005;
end

if(~exist('manual_override','var'))
    manual_override = 0;
end

if(~exist('take_larger','var'))
    take_larger = 1;
end


column_numbers = (size(dataframe,2)+1):size(dataframe,2)+size(columns_to_add,2);

size_discrepancy = (size(columns_to_add,1)-size(dataframe,1));
size_discrepancy_ratio = abs(size_discrepancy)/size(dataframe,1);
%in case there is a size discrepancy (e.g. the velocity features are 1
%shorter than the features they're derived from, etc...)

if(size_discrepancy_ratio>discrepancy_tolerance_ratio && ~isempty(dataframe))
    disp('add_to_dataframe() - there is a large size discrepancy between columns_to_add, and the existing dataframe.')
    disp(['dataframe size: ' num2str(size(dataframe,1)) ' x ' num2str(size(dataframe,2))]);
    disp(['columns_to_add size: ' num2str(size(columns_to_add,1)) ' x ' num2str(size(columns_to_add,2))]);

    if(manual_override==0)
        while(1)
            inp = input('Continue? y/n: ','s');
            if isempty(inp) || ~( strcmp(inp,'y') || strcmp(inp,'n'))
                disp('Please enter y or n')
            else
                if(strcmp(inp,'y'))
                    break;
                elseif(strcmp(inp,'n'))
                    error('add_to_dataframe operation aborted.')
                end
            end
        end
    elseif(manual_override==1)
        if(take_larger)
           if(size_discrepancy<0)
                updated_dataframe = dataframe;
                column_numbers = [];
                return;
           else
                updated_dataframe = columns_to_add;
                column_numbers = 1:size(columns_to_add,2);
                return;
           end
        else
           if(size_discrepancy>0)
                updated_dataframe = dataframe;
                column_numbers = [];
                return;
           else
                updated_dataframe = columns_to_add;
                column_numbers = 1:size(columns_to_add,2);
                return;
           end
        end
        
    elseif((manual_override==-1))
        updated_dataframe = dataframe;
        column_numbers = [];
        return;
    end
    
end

if(size_discrepancy>0 && ~isempty(dataframe))
   
    dataframe = [dataframe;repmat(dataframe(end,:),abs(size_discrepancy),1)];

elseif(size_discrepancy<0 && ~isempty(dataframe))

    columns_to_add = [columns_to_add;repmat(columns_to_add(end,:),abs(size_discrepancy),1)];

end




updated_dataframe = [dataframe columns_to_add];
