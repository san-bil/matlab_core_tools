function col=get_dataframe_column(col_key,data_dictionary,dataframe)

col_idx = kv_get(col_key,data_dictionary);
col = dataframe(:,col_idx);

end
