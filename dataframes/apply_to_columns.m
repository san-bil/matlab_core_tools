function [dataframe,data_dictionary] = apply_to_columns(columns_name,data_dictionary,func_handle,dataframe)

columns_idx = kv_get(columns_name,data_dictionary);
cols = dataframe(:,columns_idx);
[modified_cols] = func_handle(cols);
dataframe(:,columns_idx) = modified_cols;
