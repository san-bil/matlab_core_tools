function out = get_dataframe_sequence_lengths(data_dictionary, dataframe)

sequence_col = dataframe(:,kv_get('sequence_idx',data_dictionary));
out = get_sequence_lengths(sequence_col);