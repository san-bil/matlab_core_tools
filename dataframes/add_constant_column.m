function [out,idx] = add_constant_column(in, val)

% in:
%
% out:
%
% desc:
%
% tags:


out = [in ones(size(in,1),1)*val];
idx = size(out,2);
end