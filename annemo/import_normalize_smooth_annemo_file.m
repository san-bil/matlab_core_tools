function [interpolated_vals, interpolated_timestamps, smoothed_uni_signal] = import_normalize_smooth_annemo_file(filename, frame_rate)



f_contents = import_annemo_file(filename);
isplaying_indicator_col = cellfun(@(tmp)strcmp(tmp,'true'), f_contents(:,11));
[clean_values,clean_timestamps] = filter_timeseries_for_repeats_backwards(cell2mat(f_contents(isplaying_indicator_col,9)),cell2mat(f_contents(isplaying_indicator_col,10)));
if(length(clean_timestamps)>50)
    [interpolated_vals, interpolated_timestamps] = temporally_normalize_observations(clean_values, clean_timestamps, frame_rate);

    odd_maker = @(tmp)2*floor(tmp/2)+1;
    if(length(interpolated_vals)>frame_rate*2)
        smoothed_uni_signal = sgolayfilt(interpolated_vals, 3, odd_maker(frame_rate));
    else
        smoothed_uni_signal = [];
    end
else
    interpolated_vals=[];
    interpolated_timestamps=[];
    smoothed_uni_signal=[];
    
end