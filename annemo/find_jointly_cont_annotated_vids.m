function [jointly_annotated_vids, all_annotated_vids, all_files]=find_jointly_cont_annotated_vids(anno_names, root_anno_path)

all_files = {};
all_annotated_vids = {};
jointly_annotated_vids = {};

video_getter = @(tmp)(index_cellarray(strsplit(basename(tmp),'-'),1));

for i=1:length(anno_names)
    tmp_files = find_file_from_regex_recursive(root_anno_path, [anno_names{i} '.csv']);
    all_files = kv_set(anno_names{i}, tmp_files ,all_files);

    tmp_vids = my_unique(cellfun(video_getter,tmp_files,'UniformOutput',0));
    all_annotated_vids = kv_set(anno_names{i}, tmp_vids ,all_annotated_vids);
    
    if(i==1)
        jointly_annotated_vids=tmp_vids;
    else
        jointly_annotated_vids = intersect(jointly_annotated_vids,tmp_vids); 
    end
    
end