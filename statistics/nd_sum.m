function [ tmp_sum ] = nd_sum( arr )

% in: a multidimensional array
%
% out: the sum of all elements in the array
%
% desc: As above. 
%
% tags: #sum #fold #reduce #collapse #multidimensional

tmp_sum=arr;

for i = 1:length(size(arr))
    tmp_sum=sum(tmp_sum);
end


end

