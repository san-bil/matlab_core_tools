function cosine_similiarity = nan_robust_cosine_similiarity(a,b)

% in: two vectors
%
% out: their cosine similiarity, ignoring components that are NaN in either vector
%
% desc: as above.
%
% tags: #similiarity #metric #nan #defensive #robust #cosine

a_nans = isnan(a);
b_nans = isnan(b);

a_cleans = ~b_nans;
b_cleans = ~a_nans;

both_cleans = a_cleans & b_cleans;

clean_a = a(both_cleans);
clean_b = b(both_cleans);

cosine_similiarity = (clean_a*clean_b')/(sqrt(clean_a*clean_a')*sqrt(clean_b*clean_b'));