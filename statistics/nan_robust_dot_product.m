function dot_prod = nan_robust_dot_product(a,b)

% in: two vectors
%
% out: their dot product, ignoring components that are NaN in either vector
%
% desc: as above.
%
% tags: #similiarity #metric #nan #defensive #robust #dotprod

a_nans = isnan(a);
b_nans = isnan(b);

a_cleans = ~b_nans;
b_cleans = ~a_nans;

both_cleans = a_cleans & b_cleans;

clean_a = a(both_cleans);
clean_b = b(both_cleans);

dot_prod = (clean_a*clean_b');