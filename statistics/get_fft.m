function [spec,f]=get_fft( signal,sampling_frequency )

% in: a 1D signal
%     the sampling rate
%
% out: power spectrum (bandwidth x 2 matrix)
%
% desc: Plots the fourier transform of a 1D signal
%
% tags: #fourier #signal #spectrum #spectral
L=length(signal);

NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(signal,NFFT)/L;
f = (sampling_frequency/2)*linspace(0,1,NFFT/2+1);
spec=(2*abs(Y(1:NFFT/2+1)));
end

