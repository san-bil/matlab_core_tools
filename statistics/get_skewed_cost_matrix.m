function cost_matrix = get_skewed_cost_matrix( ground_truth_col )

% in: a vector of binary values, representing class labels
%
% out: a cost matrix proportional to the imbalance of the class labels
% so 
%
% desc: as above.
%
% tags: #cost #labels #costmatrix #risk #riskmatrix #groundtruth


relative_class_sizes = [(1-sum(ground_truth_col)/length(ground_truth_col)), sum(ground_truth_col)/length(ground_truth_col)];

[minority_class_label, majority_class_label] = get_minority_class_label(ground_truth_col);

class_imbalance_ratio = relative_class_sizes(majority_class_label+1)/relative_class_sizes(minority_class_label+1);

if(majority_class_label==0)
    cost_matrix = [1, 1;class_imbalance_ratio, 1];
else
    cost_matrix = [1, 1;class_imbalance_ratio, 1];
end


end

