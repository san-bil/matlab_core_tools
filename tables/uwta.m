function out = uwta(table)
%unwrap table array
out = index_cellarray(table2array(table),1);