function return_val = unwrap_table(my_table)

cell_arr = table2cell(my_table);
return_val = cell_arr{1};