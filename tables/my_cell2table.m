function tab = my_cell2table(cell_array)

tab = cell2table(cell_array(2:end,2:end),'VariableNames',cell_array(1,2:end),'RowNames',cell_array(2:end,1)');